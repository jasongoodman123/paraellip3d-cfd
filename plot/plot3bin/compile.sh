#!/bin/sh
#platform=soilblast
platform=micromorph

if [ $platform == "soilblast" ]; then 
 module load gcc-9.2.0
 g++ -o plot3bin       plot3bin.cpp -I/usr/local/tecplot/360ex_2017r3/include -L/usr/local/tecplot/360ex_2017r3/bin/ -ltecio -lstdc++ -Wl,-rpath=/usr/local/tecplot/360ex_2017r3/bin/:/usr/local/gcc-9.2.0/lib64
 #g++ -o plot3bin.szplt plot3bin.cpp -I/usr/local/tecplot/360ex_2017r3/include -L/usr/local/tecplot/360ex_2017r3/bin/ -ltecio -lstdc++ -Wl,-rpath=/usr/local/tecplot/360ex_2017r3/bin/:/usr/local/gcc-9.2.0/lib64
elif [ $platform == "micromorph" ]; then
 module load gcc-11.2.0
 g++ -o plot3bin       plot3bin.cpp -I/usr/local/tecplot/360ex_2021r1/include -L/usr/local/tecplot/360ex_2021r1/bin/ -ltecio -lstdc++ -Wl,-rpath=/usr/local/tecplot/360ex_2021r1/bin/:/usr/local/gcc-11.2.0/lib64
fi

