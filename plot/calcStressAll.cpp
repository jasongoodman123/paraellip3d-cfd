#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <cmath>

using namespace std;

const double PI = 3.1415927;
static const int OWID = 15;
static const int OPREC= 6;

int main(int argc, char *argv[])
{
  if(argc == 1) {
    cout << endl 
	 << "-- Calculate stress tensor using internal and boundary contact info --"<<endl
	 << "Usage:" << endl
	 << "1) process a single snapshot:  calcStressAll contact_file boundary_file" << endl
	 << "   --example: calcStressAll triaxial_contact_008 triaxial_bdrycntc_008 (NOT triaxial_contact_008.dat triaxial_bdrycntc_008.dat)" << endl
	 << "2) process multiple snapshots: calcStressAll contact_file_prefix boundary_file_prefix  first_suffix  last_suffix  suffix_increment" << endl
	 << "   --example: calcStressAll triaxial_contact triaxial_bdrycntc  1  100  5" << endl << endl;
    return -1;
  }	

  int first, last, incre;
  if(argc == 3) {
    first = 0;
    last  = 1;
    incre = 2;
  } else if (argc == 6) {
    first = atoi(argv[3]);
    last  = atoi(argv[4]);
    incre = atoi(argv[5]);
  }

  ifstream ifs, ifs2;
  ofstream ofs;
  char filein[50], filein2[50];
  char fileout[50];
  char num[4], s[20], snum[20];
  double stress[3][3];

  for(int snapshot = first; snapshot <= last; snapshot += incre) {
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
	stress[i][j] = 0;
    
    if(argc == 3) {
      strcpy(filein, argv[1]);
      strcpy(filein2, argv[2]);
      strcpy(fileout, argv[1]);
      strcat(fileout, "_stress");      
    } else {
      sprintf(num, "%03d", snapshot);
      strcpy(filein, argv[1]);
      strcat(filein, "_");
      strcat(filein, num);
      strcpy(filein2, argv[2]);
      strcat(filein2, "_");
      strcat(filein2, num);
      strcpy(fileout, argv[1]);
      strcat(fileout, "_stress_");
      strcat(fileout, num);      
    }

    cout << "generating file " << fileout << " ......" <<endl;

    ifs.open(filein);
    if(!ifs)  { cout<<"ifs stream error!"<<endl; exit(-1);}
    ifs2.open(filein2);
    if(!ifs2) { cout<<"ifs2 stream error!"<<endl; exit(-1);}
    ofs.open(fileout);
    if(!ofs)  { cout<<"ofs stream error!"<<endl; exit(-1);}
    ofs.setf(ios::scientific, ios::floatfield);

    int internalContact;
    ifs >> internalContact;
    //ofs << "internalContact=" << internalContact << std::endl;
    ifs >> s >> s >> s >> s >> s >> s >> s >> s >> s >> s
		>> s >> s >> s >> s >> s >> s >> s >> s >> s >> s
		>> s >> s >> s >> s >> s >> s >> s >> s >> s >> s
		>> s >> s >> s >> s >> s;

    int ptcl_1;
    int ptcl_2;
    double point1_x;
    double point1_y;
    double point1_z;
    double point2_x;
    double point2_y;
    double point2_z;
    double radius_1;
    double radius_2;
    double penetration;
    double tangt_disp;
    double contact_radius;
    double R0;
    double E0;
    double normal_force;
    double tangt_force;
    double normal_all;
    double contact_x;
    double contact_y;
    double contact_z;
    double normal_x;
    double normal_y;
    double normal_z;
    double tangt_x;
    double tangt_y;
    double tangt_z;
    double vibra_t_step;
    double impact_t_step;
    double dir_x;
    double dir_y;
    double dir_z;
    double fx, fy, fz;
    double branch_x, branch_y, branch_z;
    
    for(int it = 0; it < internalContact; ++it) {
     ifs >> ptcl_1
	 >> ptcl_2
	 >> point1_x
	 >> point1_y
	 >> point1_z
	 >> point2_x
	 >> point2_y
	 >> point2_z
	 >> radius_1
	 >> radius_2
	 >> penetration
	 >> tangt_disp
	 >> contact_radius
	 >> R0
	 >> E0
	 >> normal_force
	 >> tangt_force
	 >> normal_all
	 >> contact_x
	 >> contact_y
	 >> contact_z
	 >> normal_x
	 >> normal_y
	 >> normal_z
	 >> tangt_x
	 >> tangt_y
	 >> tangt_z
	 >> vibra_t_step
	 >> impact_t_step
	 >> dir_x
	 >> dir_y
	 >> dir_z
	 >> branch_x >> branch_y >> branch_z;
     //ofs << std::setw(OWID) << branch_x << std::setw(OWID) << branch_y << std::setw(OWID) << branch_z << std::endl;
     fx = normal_x + tangt_x;
     fy = normal_y + tangt_y;
     fz = normal_z + tangt_z;
     stress[0][0] += fx * branch_x; stress[0][1] += fx * branch_y; stress[0][2] += fx * branch_z;
     stress[1][0] += fy * branch_x; stress[1][1] += fy * branch_y; stress[1][2] += fy * branch_z;
     stress[2][0] += fz * branch_x; stress[2][1] += fz * branch_y; stress[2][2] += fz * branch_z;	 
    }

    int bdryIndex, bdryPointNum;
    double pos_x, pos_y, pos_z, pentr, volume;    
    int totalBdryNum;
    ifs2 >> totalBdryNum;
    //ofs << "totalBdryNum=" << totalBdryNum << std::endl;
    for (int bdryNum = 1; bdryNum <= totalBdryNum; ++bdryNum) {
      ifs2 >> bdryIndex >> bdryPointNum >> s >> s >> s >> s >> s >> s >> s >> s >> s >> s >> s >> s >> s;
      //ofs << "bdryPointNum=" << bdryPointNum << std::endl;
      for (int pointNum = 0; pointNum < bdryPointNum; ++pointNum) {
	ifs2 >> pos_x >> pos_y >> pos_z >> normal_x >> normal_y >> normal_z >> tangt_x >> tangt_y >> tangt_z >> pentr >> branch_x >> branch_y >> branch_z;
	//ofs << std::setw(OWID) << branch_x << std::setw(OWID) << branch_y << std::setw(OWID) << branch_z << std::endl;
	fx = -(normal_x + tangt_x);
	fy = -(normal_y + tangt_y);
	fz = -(normal_z + tangt_z);
	stress[0][0] += fx * branch_x; stress[0][1] += fx * branch_y; stress[0][2] += fx * branch_z;
	stress[1][0] += fy * branch_x; stress[1][1] += fy * branch_y; stress[1][2] += fy * branch_z;
	stress[2][0] += fz * branch_x; stress[2][1] += fz * branch_y; stress[2][2] += fz * branch_z;	
      }
      ifs2 >> normal_x >> normal_y >> normal_z >> tangt_x >> tangt_y >> tangt_z >> pentr;
    }
    ifs2 >> volume;
    ofs << "volume=" << volume << std::endl;
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
	stress[i][j] /= volume;
    
    ofs << std::setw(OWID) << std::left << "sigma=[ ..." << std::right << std::endl;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j)
	ofs << std::setw(OWID) << stress[i][j];
      if (i < 2) ofs << ";"; else ofs << "]";
      ofs << std::endl;
    }
  
    ifs.close();
    ifs2.close();
    ofs.close();

  }
    
  return 0;
}
