# Technical notes
[[_TOC_]]

##  Simulation types
The simulation types include:
  * 001 - proceed simulation with preset state
  * 002 - tune up mass percentage from number percentage
  * 003 - trim particles to a certain space domain
  * 004 - remove particles inside a spherical domain
  * 005 - calculate mass percentage from particle file
  * 101 - deposit spatially scattered particles into a rigid container
  * 102 - resume deposition using specified data file of particles and boundaries
  * 111 - move 4 side walls at constant velocities
  * 112 - move 4 side walls and top wall at constant velocities
  * 201 - isotropic 1, low confining pressure
  * 202 - isotropic 2, increasing confining pressure
  * 203 - isotropic 3, loading-unloading-reloading
  * 301 - oedometer 1
  * 302 - oedometer 2, loading-unloading-reloading
  * 401 - triaxial 1
  * 402 - triaxial 2, loading-unloading-reloading
  * 411 - plane strain 1, in x direction
  * 412 - plane strain 2, loading-unloading-reloading
  * 501 - true triaxial 1, create confining stress state
  * 502 - true triaxial 2, increase stress in one direction
  * 601 - expand particles volume inside a virtual cavity
  * 602 - resume simulation after expanding particles volume inside a virtual cavity
  * 701 - couple with supersonic air flow, bottom "left" part, R-H conditions
  * 702 - couple with supersonic air flow, bottom "left" part
  * 703 - couple with supersonic air flow, rectangular "left" part
  * 704 - couple with supersonic air flow, spherical "left" part
  * 705 - couple with supersonic air flow, rectangular "left" part, and a zone below
  * 801 - pure supersonic air flow, bottom "left" part, R-H conditions
  * 802 - pure supersonic air flow, bottom "left" part
  * 803 - pure supersonic air flow, rectangular "left" part
  * 804 - pure supersonic air flow, spherical "left" part
  * 805 - pure supersonic air flow, rectangular "left" part, and a zone below

In these examples, the directory "long_runtime" contains simulations that are fully completed,
and the directory "short_runtime" only runs a short period of wall time for the simulations,
which are usually uncompleted but serve as a quick test. The difference lies in the
parameters in input.txt, and the job script accordingly.

The simulation types 001~005 usually involve light-weight serial computation for data 
operation, therefore only a short_runtime example is needed; other simulation types usually 
require parallel computing, thus both long_runtime and short_runtime examples are provided.

It is best for users to get started with simuType101-deposit, which generates a particle 
assembly sample through gravitational deposition simulated by parallel computing.

##  Simulation flowchart
Typically we start with generating particles according gradation curves, deposit them into a 
container through gravitational pluviation, then apply confining pressure to a certain amount,
and then do isotropic compression, triaxial compression, plane strain compression, etc.

There are other paths. For example, deposit particles to create initial samples, then remove 
boundary wall to simulate collapse process; or dig a hole inside the sample, and apply dentonated
gas there to simulate buried explosion. The particles can be applied higher gravity to model 
higher stress state if needed.

We list a typical DEM simulation flow chart here:
1. gravitational pluviation.
2. trimming the sample to desired size.
3. degravitation (optional).
4. isotropic compression with low confining pressure.
5. isotropic compression to higher confining pressure.
6. isotropic compression to even higher confining pressure.
7. do oedometer/isotropic/triaxial/plane strain/true triaxial/shear tests with samples obtained in 4, 5, 6.

and a typical DEM-CFD coupled simulation flow chart here:
1. gravitational pluviation.
2. trimming the sample to desired size.
3. apply higher gravity to satisfy stress state (optional).
4. create a cavity inside the sample to hold dentonated gas (optional).
5. set up BVs and IVs for the gas field.
6. start DEM-CFD simulations.

##  SimuType 201 (isotropic type 1)
The technical notes include:
1. have an initial gap on top, which is physical.  
2. must use boundaryRate for numerical stability.  
3. gravScale = 0, degravitation is necessary for low stresss control.  
4. boundaryFric = 0 is necessary for degravitation, otherwise stress may not be released,  
   especially for specimen of rectangular prism.
5. forceDamp = momentDamp = 0. Background mass damping  
   a) stops impulse from propagating even if it is low.  
   b) causes unreally large hysteresis loop in unloading-reloading path even if it is low.  
   c) larger value results in larger hysteresis loop.  
6. contactDamp: 0 leads to quicker motion; 1 leads to better balance.  
7. massScale/mntScale:  
   a) larger values eliminate more dynamic behavior.  
   b) larger values lead to slower but more stable balance of tractions.  
      This may not hold true for other cases like simuType202.  
   c) plot traction curves to ensure true equilibrium, this is important for subsequent simulations.  
   d) need to be increased if contact Young's modulus is increased.  
8. adjust for speedup:  
   a) tractionErrorTol: larger value allows faster equilibrium.  
   b) boundaryRate/topSpeedup: combination results in faster equilibrium.   
   c) observe the top gap, use larger topSpeedup for larger gap.  
   d) if a wide range of topSpeedup does not work, change boundaryRate. The topSpeedup only   
      accelerats top wall before it contacts particles.  
   e) in case d), increasing boundaryRate is most efficient for increasing traction Z2.  
9. Young's modulus:  
   for higher Young's modulus, use higher massSclae/mntScale and lower boundaryRate.  

Some example parameters are listed here:  

1. for 1k confining pressure  
   timeStep   5.0E-6  
   massScale  75  
   young      8.5E+9  
   1: contactDamp = 1.0, boundaryRate = 25.0e-4, topSpeedup = 200, 54948 steps  
   2: contactDamp = 0.0, boundaryRate = 10.0e-4, topSpeedup = 400, 39114 steps  
   3: contactDamp = 1.0, boundaryRate = 25.0e-4, topSpeedup = 400, 46029 steps  
   4: contactDamp = 1.0, boundaryRate = 15.0e-4, topSpeedup = 400, 32235 steps  
   
2. for 1k confining pressure  
   timeStep   1.0E-6  
   massScale  300 (400 or 500 also work, may require different topSpeedup.)  
   young      4.5E+10  
   boundaryRate  15.0E-4  
   use larger topSpeedup for larger gap, this helps most significantly to allow top wall to   
   achieve balance and consequently all walls, e.g., 400 to 1000 to 5000.  
  
3. for 100k confining pressure  
   timeStep   5.0E-6  
   massScale  10  
   young      2.9E+10  
   boundaryRate  25.0E-4  
   topSpeedup  5000  

##  SimuType 202 (isotropic type 2)
The technical notes include:
1. forceDamp = momentDamp = 0  
2. contactDamp = 1. Contact damping  
   a) does not stop impulse from propagating even if it is high.  
   b) larger values leads to less oscillation.  
   c) larger values results in larger hysteresis loop in unloading-reloading path.  
   d) hysteresis is supposed to be generated by contact damping rather than background damping.  
3. massScale/mntScale:  
    larger values lead to slightly faster performance, e.g., 50->75->100 makes 81->80->78 snapshots.  
4. may still use boundaryRate 5.0E-4, but high values like 30.0E-4 work well.  

overall, these points are important:
1. ensure there is no overlap limitation so that the relationship
between stress and normal contact force is estimated correctly.

2. in quasi-static simulation below 3500kPa, maxRelaOverlap 1% and
5% give nearly the same final state of particles.

3. for high pressure like 35MPa or abover, use high boundary rate
and accordingly high maxRelaOverlap.

## SimuType 411/412 (plane strain)
The technical notes include:
1. use smaller boundaryRate than triaxial
2. use sideRateRatio (>1) to achieve contant confining pressure in y direction.
3. combination of more steps of smaller boundaryRate (the same strain) appears to not help.


## SimuType 801-805 and 701-705 (Gas and Couple)

The explicit DEM is very sensitive to time step because its contact model needs 
to detect tiny overlap between particles, which is usually at the length scale 
of 1e-6 meter or below for sand grains. Although it is able to allow adaptive 
time steps, the DEM usually uses a fixed time step for improved numerical stability. 

The CFD is different in that it has a more accurate control over time step 
utilizing CFL condition, for both linear and nonlinear problem.

So the right way to set up time step in ParaEllip3d is as follows:

  * To use fixed small time steps for analysis, specify a small time step and 
larger CFL coefficient (if it exists). If this time step is smaller than 
what CFL esitmates, it will not be changed in successive computation.

  * To use adaptive time steps for long time computation, specify a large time 
step and small CFL coefficient (if it exists).

then check debugInf file to ensure time step is correctly set up. If such 
errors as "UtoW:prs<0" are printed indicating absolute negative pressure, 
smaller CFL must be used.

A typical CFL value for gas simulation of shock wave is 0.5; and a typical CFL 
value for gas-particle interaction simulation in shock wave is 0.25. When adaptive 
time steps are adopted, trial and error of CFL is necessary in checking the 
early time steps until the maximum CFL is found. Athough it is tedious, this trial 
and error would save a lot of computational cost for long time simulations. 

For a typical spherical blast wave, earlier computational time step is smaller 
than later time step because of the higher pressure gradient at earlier time. An 
appropriate CFL value obtained from trial and error not only guarantees numerical 
stability but also allows adaptive time step for computational efficiency. Although 
a dynamically adaptive CFL seems to be the best choice, this is not an option in 
the code.
