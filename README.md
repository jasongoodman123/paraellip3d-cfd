# ParaEllip3d-CFD

[[_TOC_]]

## Introduction

ParaEllip3D-CFD is a parallel DEM and parallel DEM-CFD coupled simulation code that is capable of 
modeling a large number of spherical and non-spherical (ellipsoidal and poly-ellipsoidal) particles 
and their interaction with ambient air environment. The software is highly scalable and portable with 
current supercomputers. For example, it has run across thousands of nodes on many DOD/DOE supercomputers 
for DEM and DEM-CFD simulations.

ParaEllip3d-CFD is not designed to replace the classical molecular and granular 
simulation package LAMMPS (www.lammps.org), but provides its unique feature and capability:

1. Handling non-spherical grains using ellipsoidal and poly-ellipsoidal shapes.
2. Replicating history-dependent Mindlin tangential contact model for granular materials.
3. Simulating tens of laboratory and in-site tests: oedometer compression, triaxial and true triaxial compression, plane-strain compression, direct simple shear, dynamic penetration, buried explosion, lunar dust mitigation, etc.
4. Modeling binder/grain composites or mixture using pure DEM.
5. Upscaling to stress and deformation measures within nonlinear continuum mechanics.
6. Providing multiple CFD solvers: Lax-Friedrichs scheme, Lax-Wendroff scheme, Riemann solvers (Roe, HLLC, HLLE, Exact, etc).
7. Computing resolved DEM-CFD coupling simulations for particle-gas interaction.

It supports various types of simulations:

1.  Particle number/size/mass distribution analysis and adjustment.
1.  Dual-particle interactive sticking, sliding, rolling and impact.
1.  Gravitational deposition.
1.  Degravitation response.
1.  Isotropic compression.
1.  Oedometer compression.
1.  Conventional triaxial compression.
1.  True triaxial compression.
1.  Plane-strain compression.
1.  Simple shear.
1.  Triaxial compression membrane simulation.
1.  Compressive and shear wave propagation.
1.  Mechanical volume expansion.
1.  Dynamic and quasi-static penetration.
1.  Gravitational collapse.
1.  Boundary wall movement.
1.  Mechanical coupling with Finite Element Method (FEM).
1.  CFD solvers: Lax-Friedrichs scheme, Lax-Wendroff scheme, Riemann
    solvers (Roe, HLLC, HLLE, Exact and the smart combinations).
1.  Two-way Mechanical coupling with Computational Fluid Dynamics (CFD).
1.  Universal Four-layer Synchronized Communication (UFLSC) Computational 
    Framework for resolved parallel DEM-CFD coupled computation.
    
    
To be more specific, ParaEllip3d-CFD defines the following simulation types, 
which allow users to create variants of configuration according to their need.
  * 001 - proceed simulation with preset state
  * 002 - tune up mass percentage from number percentage
  * 003 - trim particles to a certain space domain
  * 004 - remove particles inside a spherical domain
  * 005 - calculate mass percentage from particle file
  * 101 - deposit spatially scattered particles into a rigid container
  * 102 - resume deposition using specified data file of particles and boundaries
  * 111 - move 4 side walls at constant velocities
  * 112 - move 4 side walls and top wall at constant velocities
  * 201 - isotropic 1, low confining pressure
  * 202 - isotropic 2, increasing confining pressure
  * 203 - isotropic 3, loading-unloading-reloading
  * 301 - oedometer 1
  * 302 - oedometer 2, loading-unloading-reloading
  * 401 - triaxial 1
  * 402 - triaxial 2, loading-unloading-reloading
  * 411 - plane strain 1, in x direction
  * 412 - plane strain 2, loading-unloading-reloading
  * 421 - simple shear 1, fixed height
  * 422 - simple shear 2, constant confining pressure
  * 423 - simple shear 3, assigned boundary displacement
  * 424 - simple shear 4, top/bottom boundary horizontal shearing
  * 501 - true triaxial 1, create confining stress state
  * 502 - true triaxial 2, increase stress in one direction
  * 601 - expand particles volume inside a virtual cavity
  * 602 - resume simulation after expanding particles volume inside a virtual cavity
  * 701 - couple with supersonic air flow, bottom "left" part, R-H conditions
  * 702 - couple with supersonic air flow, bottom "left" part
  * 703 - couple with supersonic air flow, rectangular "left" part
  * 704 - couple with supersonic air flow, spherical "left" part
  * 705 - couple with supersonic air flow, rectangular "left" part, and a zone below
  * 801 - pure supersonic air flow, bottom "left" part, R-H conditions
  * 802 - pure supersonic air flow, bottom "left" part
  * 803 - pure supersonic air flow, rectangular "left" part
  * 804 - pure supersonic air flow, spherical "left" part
  * 805 - pure supersonic air flow, rectangular "left" part, and a zone below

## Simulation examples and movies

Below are some YouTube movies demonstrating simulation results performed
by ParaEllip3D-CFD:

1.  DEM simulations:

  * Gravitational deposition of one million polydisperse ellipsoid particles into a container.  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vVeXfGMKS6zZeikWT55l-ew
  * Constrained collapse of four different shapes of particles.  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vXpHkTKdPz-KoZNtJ6wHees
  * Gravitational packing of polydisperse spherical particles.  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vU93mgIYk16ItMTCoO9WDMx
  * Gravitational packing of polydisperse ellipsoidal particles.   
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vX6JnnVkKpLvEeLEPK24hWU
  * Triaxial compression test with ellipsoidal particles.  
    https://youtube.com/playlist?list=PL0Spd0Mtb6vXM2BwnQO6elSXzxMIpbQRx
  * Field variables in triaxial compression test with ellipsoidal particles.  
    https://youtube.com/playlist?list=PL0Spd0Mtb6vUOzd6bD9j_0d4OHaAUvRwj
  * Penetration into particle assemblies.  
    https://youtube.com/playlist?list=PL0Spd0Mtb6vX5fPecA7nRjrwpPJ4d6pX1
  * Dual-particle centric impact with varying interparticle contact damping (slow motion).  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vVKIr5oLpF079X7KoZNuWA0
  * Dual-particle ecentric impact with varying interparticle contact damping (slow motion).  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vU5L6BV1YgfHRJI1tM5YqDY
  * Particle rolling on another inside a box.  
    https://youtube.com/playlist?list=PL0Spd0Mtb6vVbLqseNIRG7LFNmRL0Oath

2.  Stress/strain in DEM:

  * Stress in the process of constrained collapse of four different shapes of particles.  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vWM7MVqTjhUDgVUF7LA9vcB
  * Stress in the process of gravitational packing of particles.  
    https://www.youtube.com/playlist?list=PL0Spd0Mtb6vXWB2qqR0k6Hc35Qcj12tXr

3.  DEM-CFD coupled simulation of buried soil explosion:

  * Depth of Burial (DOB) effect on gas pressure of sand-buried explosion of 100-gram charge.  
    https://youtu.be/7Bi3q91UZ5Q
  * Depth of Burial (DOB) effect on Mach number of sand-buried explosion of 100-gram charge.  
    https://youtu.be/vuBsyDo97yw
  * Depth of Burial (DOB) effect on particle motion (mask) of sand-buried explosion of 100-gram charge.  
    https://youtu.be/cKr7OA1L9cw
  * Depth of Burial (DOB) effect on particle motion (dot) of sand-buried explosion of 100-gram charge.  
    https://youtu.be/YebMLsaXPMs 
  * Depth of Burial (DOB) effect on particle velocity field of sand-buried explosion of 100-gram charge.  
    https://youtu.be/ZJ6zYBpqmG0
    
## Building on supercomputers

ParaEllip3d-CFD uses the following open source libraries:

1. [Boost](https://www.boost.org) (required), a set of C++ libraries for tasks 
and structures such as pseudorandom number generation and message passing in 
high performance parallel applications;
1. [EIGEN](http://eigen.tuxfamily.org) (required), a C++ template library for 
linear algebra such as matrices, vectors, numerical solvers, and related algorithms;
1. [QHULL](http://www.qhull.org/) (required), a library for computing the convex 
hull, Delaunay triangulation, Voronoi diagram, etc;
1. [PAPI](https://icl.utk.edu/papi) (optional), a library for use of the 
performance counter hardware found in most major microprocessors.

The four libraries should be installed before building ParaEllip3d-CFD. Note that 
the four libraries and paraEllip3d-CFD should be built based on the same environment 
modules. Hereby, we emphasize the importance to use the consistent modules/libraries 
in the following three steps, otherwise the code may not run correctly:
1. build the four libraries.
1. build ParaEllip3d-CFD.
1. prepare job scripts.

We demonstrate the building process of ParaEllip3d-CFD on LLNL Quartz (quartz.llnl.gov) 
using different MPI implementations: OpenMPI, Intel MPI and Mvapich2. Similarly, it can 
be done on well-configured local workstations.

The installation directory of the four libraries can be customized, for example, 
we can specify it using the following command:
```console
$ export LOCAL_INSTALL=/g/g92/yan15/local (use your own directory on Quartz)
```

### OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3)

#### Install boost C++

1\. Load module environment:
```console
$ module swap intel gcc/4.9.3
$ module swap mvapich2 openmpi/4.0.0
$ which gcc mpic++
/usr/tce/packages/gcc/gcc-4.9.3/bin/gcc
/usr/tce/packages/openmpi/openmpi-4.0.0-gcc-4.9.3/bin/mpic++
```

2\. Download and extract the Boost C++ tarball:
```console
$ wget https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.tar.gz
$ tar xzf boost_1_75_0.tar.gz
$ cd boost_1_75_0
```

3\. Configure Boost:
```console
$ ./bootstrap.sh \
  --prefix=$(LOCAL_INSTALL)/boost-1.75.0-openmpi-4.0.0-gcc-4.9.3 \
  --with-libraries=date_time \
  --with-libraries=filesystem \
  --with-libraries=mpi \
  --with-libraries=program_options \
  --with-libraries=random \
  --with-libraries=regex \
  --with-libraries=serialization \
  --with-libraries=system \
  --with-libraries=timer
```

4\. Edit the build script to use loaded MPI:
```console
$ echo 'using mpi : /usr/tce/packages/openmpi/openmpi-4.0.0-gcc-4.9.3/bin/mpic++ ;' >> ./tools/build/example/user-config.jam
```

5\. Build:
```console
$ ./b2 -j 32 --user-config=./tools/build/example/user-config.jam install
```

#### Install Qhull

```console
$ module swap intel gcc/4.9.3
$ wget http://www.qhull.org/download/qhull-2020-src-8.0.2.tgz
$ tar xzf qhull-2020-src-8.0.2.tgz
$ mv qhull-2020.2 qhull-2020.2-gcc-4.9.3
$ cd qhull-2020.2-gcc-4.9.3
$ make
$ cd ..
$ mv qhull-2020.2-gcc-4.9.3 $(LOCAL_INSTALL)
```

#### Install Eigen
Note that no compile is needed for Eigen because they are C++ header files.
```console
$ wget https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.gz
$ tar xzf eigen-3.3.9.tar.gz
$ mv eigen-3.3.9.tar.gz $(LOCAL_INSTALL)
```

#### Install PAPI (optional)
It will be installed for performance evaluation.

### Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4)

#### Install boost C++

1\. Load module environment:
```console
$ module swap mvapich2 impi/2019.8
$ which icpc mpiicpc
/usr/tce/packages/intel/intel-19.0.4/bin/icpc
/usr/tce/packages/impi/impi-2019.8-intel-19.0.4/bin/mpiicpc
```

2\. Download and extract the Boost C++ tarball:
```console
$ wget https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.tar.gz
$ tar xzf boost_1_75_0.tar.gz
$ cd boost_1_75_0
```

3\. Configure Boost:
```console
$ export CC=icc 
$ export CXX="icpc -std=c++11"
$ export CXXFLAGS=-std=c++11
$ export B2_CXX="icpc -std=c++11"
$ ./bootstrap.sh \
  --prefix=$(LOCAL_INSTALL)/boost-1.75.0-impi-2019.8-intel-19.0.4 \
  --with-toolset=intel-linux \
  --with-libraries=date_time \
  --with-libraries=filesystem \
  --with-libraries=mpi \
  --with-libraries=program_options \
  --with-libraries=random \
  --with-libraries=regex \
  --with-libraries=serialization \
  --with-libraries=system \
  --with-libraries=timer
```

4\. Edit the build script to use loaded MPI:
```console
$ echo 'using mpi : /usr/tce/packages/impi/impi-2019.8-intel-19.0.4/bin/mpiicpc ;' >> ./tools/build/example/user-config.jam
```

5\. Build:
```console
$ ./b2 -j 32 toolset=intel cflags="-lmpi_mt" cxxflags="-lmpi_mt" linkflags="-lmpi_mt" --user-config=./tools/build/example/user-config.jam install
```

#### Install Qhull
Use default module intel/19.0.4
```console
$ wget http://www.qhull.org/download/qhull-2020-src-8.0.2.tgz
$ tar xzf qhull-2020-src-8.0.2.tgz
$ mv qhull-2020.2 qhull-2020.2-intel-19.0.4
$ cd qhull-2020.2-intel-19.0.4
$ make CC=icc CXX=icpc
$ cd ..
$ mv qhull-2020.2-intel-19.0.4 $(LOCAL_INSTALL)
```

#### Install Eigen
Note that no compile is needed for Eigen because they are C++ header files.
```console
$ wget https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.gz
$ tar xzf eigen-3.3.9.tar.gz
$ mv eigen-3.3.9.tar.gz $(LOCAL_INSTALL)
```

#### Install PAPI (optional)
It will be installed for performance evaluation.

### Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2)

#### Install boost C++

1\. Load module environment by default:
```console
$ module swap intel/19.0.4 intel/2021.2
$ module list
Currently Loaded Modules:
  1) texlive/2016   2) StdEnv (S)   3) intel/2021.2   4) mvapich2/2.3.6
$ which icpc mpic++
/usr/tce/packages/intel/intel-2021.2/bin/icpc
/usr/tce/packages/mvapich2/mvapich2-2.3.6-intel-2021.2/bin/mpic++
```

2\. Download and extract the Boost C++ tarball:
```console
$ wget https://boostorg.jfrog.io/artifactory/main/release/1.76.0/source/boost_1_76_0.tar.gz
$ tar xzf boost_1_76_0.tar.gz
$ cd boost_1_76_0
```

3\. Configure Boost:
```console
$ export CC=icc 
$ export CXX="icpc -std=c++11"
$ export CXXFLAGS=-std=c++11
$ export B2_CXX="icpc -std=c++11"
$ ./bootstrap.sh \
  --prefix=$(LOCAL_INSTALL)/boost-1.76.0-mvapich2-2.3.6-intel-2021.2 \
  --with-toolset=intel-linux \
  --with-libraries=date_time \
  --with-libraries=filesystem \
  --with-libraries=mpi \
  --with-libraries=program_options \
  --with-libraries=random \
  --with-libraries=regex \
  --with-libraries=serialization \
  --with-libraries=system \
  --with-libraries=timer
```

4\. Edit the build script to use loaded MPI:
```console
$ echo 'using mpi : /usr/tce/packages/mvapich2/mvapich2-2.3.6-intel-2021.2/bin/mpic++ ;' >> ./tools/build/example/user-config.jam
```

5\. Build:
```console
$ ./b2 -j 32 toolset=intel cflags="-lmpi" cxxflags="-lmpi" linkflags="-lmpi" --user-config=./tools/build/example/user-config.jam link=shared install
```
or
```console
$ ./b2 -j 32 toolset=intel cflags="-lmpi_mt" cxxflags="-lmpi_mt" linkflags="-lmpi_mt" --user-config=./tools/build/example/user-config.jam link=shared install
```
The `_mt` (multithreaded) may not may not affect the compile and performance.

#### Install Qhull
```console
$ module swap intel/19.0.4 intel/2021.2
$ wget http://www.qhull.org/download/qhull-2020-src-8.0.2.tgz
$ tar xzf qhull-2020-src-8.0.2.tgz
$ mv qhull-2020.2 qhull-2020.2-intel-2021.2
$ cd qhull-2020.2-intel-2021.2
$ make CC=icc CXX=icpc
$ cd ..
$ mv qhull-2020.2-intel-2021.2 $(LOCAL_INSTALL)
```

#### Install Eigen
Note that no compile is needed for Eigen because they are C++ header files.
```console
$ wget https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.gz
$ tar xzf eigen-3.3.9.tar.gz
$ mv eigen-3.3.9.tar.gz $(LOCAL_INSTALL)
```

#### Install PAPI (optional)
It will be installed for performance evaluation.


### Building ParaEllip3d-CFD
After the four libraries are installed, ParaEllip3d-CFD can be built accordingly 
using its `makefile`. 

#### OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3)
1\. Download ParaEllip3d-CFD from gitlab.com
```console
$ git clone git@gitlab.com:micromorph/paraellip3d-cfd
```
or
```console
$ git clone https://gitlab.com/micromorph/paraellip3d-cfd
```

2\. Load module environment:
```console
$ module swap intel gcc/4.9.3
$ module swap mvapich2 openmpi/4.0.0
```

3\. Edit `makefile` in directory `src` according to your environment. For 
example, you need to select the supercomputer platform and MPI/C++ compilers
combination. And the paths to your boost C++, Eigen, Qhull and PAPI libraries
need to be specified, which is usually done through the variable `$(LOCAL_INSTALL)` 
defined in the `makefile`. Note that the compile environment for the four libraries 
and ParaEllip3d must be conformed.

As a default example based on OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3), 
you only need to specify the following variables in the `makefile`:
```makefile
 PLATFORM = quartz 
 COMPILERS = gcc-openmpi
 LOCAL_INSTALL=/g/g92/yan15/local
 ...
 ifeq ($(COMPILERS), gcc-openmpi)
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.75.0-openmpi-4.0.0-gcc-4.9.3
  ifeq ($(EIGEN), yes)                                              
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9                        
  endif                                                             
  ifeq ($(QHULL), yes)                                              
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-4.9.3             
  endif                                                             
  ifeq ($(PAPI), yes)                                               
   PAPI_ROOT =                                                      
  endif                                                             
 endif  
```

4\. Build
```console
$ cd src
$ make
```
This will generate an executable `paraEllip3d` that conforms with OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3).

#### Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4)

Similarly, you can change from step 2 to use Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4),
and edit `makefile`, then you build a different version of `paraEllip3d`. For example,
load the environment
```console
$ module swap mvapich2 impi/2019.8
```
and edit `makefile` as follows:
```makefile
 PLATFORM = quartz 
 COMPILERS = intel-intel
 LOCAL_INSTALL=/g/g92/yan15/local
 ...
 ifeq ($(COMPILERS), intel-intel)
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.75.0-impi-2019.8-intel-19.0.4
  ifeq ($(EIGEN), yes)                                               
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9                         
  endif                                                              
  ifeq ($(QHULL), yes)                                               
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-intel-19.0.4           
  endif                                                              
  ifeq ($(PAPI), yes)                                                
   PAPI_ROOT =                                                       
  endif                                                              
 endif   
```

#### Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2)
Similarly, you can change from step 2 to use Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2),
and edit `makefile`, then you build a different version of `paraEllip3d`. For example,
use the default environment 
```console
$ module swap intel/19.0.4 intel/2021.2
$ module list
Currently Loaded Modules:
  1) texlive/2016   2) StdEnv (S)   3) intel/2021.2   4) mvapich2/2.3.6
```

and edit `makefile` as follows:
```makefile
 PLATFORM = quartz 
 COMPILERS = intel-mvapich2
 LOCAL_INSTALL=/g/g92/yan15/local
 ...
 ifeq ($(COMPILERS), intel-intel)
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.76.0-mvapich2-2.3.6-intel-2021.2
  ifeq ($(EIGEN), yes)                                               
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9                         
  endif                                                              
  ifeq ($(QHULL), yes)                                               
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-intel-2021.2           
  endif                                                              
  ifeq ($(PAPI), yes)                                                
   PAPI_ROOT =                                                       
  endif                                                              
 endif   
```

#### makefile
For your reference, below is the content of the default `makefile`, which supports 
multiple DOD/DOE supercomputer platforms:
```makefile
#///////////////////////////////////////////////////////////////////////////////////////////////////////
#//                                   Code: ParaEllip3d-CFD                                           //
#//                                 Author: Dr. Beichuan Yan                                          //
#//                                  Email: beichuan.yan@colorado.edu                                 //
#//                              Institute: University of Colorado Boulder                            //
#///////////////////////////////////////////////////////////////////////////////////////////////////////

#########################################################
###        Part A: change with your environment       ###
#########################################################
###1. system platform
#PLATFORM = soilblast
PLATFORM = micromorph
#PLATFORM = quartz
#PLATFORM = thunder
#PLATFORM = centennial
#PLATFORM = onyx
#PLATFORM = excalibur
#PLATFORM = topaz
#PLATFORM = lightning

###2. compiler combinations (c++ and mpi)
# os_default is for default packages installed on local workstations of Red Hat, Fedora, etc,
# and old OS's like Red Hat 6.10 do not support this option as their default compilers do not support C++11.
# for example, Fedora 34 default:
# dnf install openmpi
# dnf install openmpi-devel
# dnf install boost-openmpi
# dnf install boost-openmpi-devel

#COMPILERS = os_default
COMPILERS = gcc-openmpi
#COMPILERS = intel-mvapich2
#COMPILERS = gcc-mvapich2
#COMPILERS = intel-sgi
#COMPILERS = intel-cray
#COMPILERS = intel-intel
#COMPILERS = gcc-sgi
#COMPILERS = gcc-cray

###3. locally installed prerequisite libraries
# for local workstations
LOCAL_INSTALL=/usr/local
# for supercomputers
#LOCAL_INSTALL=/g/g92/yan15/local

###4. compute continuum_stress-strain or not?
STRESS_STRAIN = yes
ifeq ($(STRESS_STRAIN), yes)
 EIGEN = yes
 QHULL = yes
 MACRO_STRESS_STRAIN = -DSTRESS_STRAIN
endif

###5. print contact info or not?
#PRINT_CONTACT = yes
ifeq ($(PRINT_CONTACT), yes)
 MACRO_PRINT_CONTACT = -DPRINT_CONTACT
endif

###6. use papi or not?
#PAPI = yes

###7. boost location
ifeq ($(PLATFORM), soilblast)
 ifeq ($(COMPILERS), os_default)
  #this option does not work, it is only a placeholder.
  #no need to specify BOOST_ROOT
  #export PATH=/usr/lib64/openmpi/bin/:$PATH
  ifeq ($(PAPI), yes)
   PAPI_ROOT = $(LOCAL_INSTALL)/papi-6.0.0...
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2015
  endif
 endif

 ifeq ($(COMPILERS), gcc-openmpi)
  #module load openmpi-4.0.2-gcc-9.2.0
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.70.0-openmpi-4.0.2-gcc-9.2.0
  ifeq ($(PAPI), yes)
   PAPI_ROOT = $(LOCAL_INSTALL)/papi-6.0.0-openmpi-4.0.2-gcc-9.2.0
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2015.2-gcc-9.2.0
  endif
 endif
 
 ifeq ($(COMPILERS), intel-intel)
  BOOST_ROOT = /usr/local/boost-1.66.0-intel-psxe-2018
  ifeq ($(PAPI), yes)
   PAPI_ROOT = 
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /usr/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /usr/local/qhull-2015.2-intel-psxe-2018
  endif
 endif
 
endif

ifeq ($(PLATFORM), micromorph)
 ifeq ($(COMPILERS), os_default)
  #no need to specify BOOST_ROOT
  #export PATH=/usr/lib64/openmpi/bin/:$PATH
  ifeq ($(PAPI), yes)
   PAPI_ROOT = $(LOCAL_INSTALL)/papi-6.0.0...
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-11.1.1
  endif
 endif

 ifeq ($(COMPILERS), gcc-openmpi)
  #module load openmpi-4.1.1-gcc-11.2.0
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0
  ifeq ($(PAPI), yes)
   PAPI_ROOT = $(LOCAL_INSTALL)/papi-6.0.0-openmpi-4.1.1-gcc-11.2.0
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-11.2.0
  endif
 endif

 ifeq ($(COMPILERS), gcc-mvapich2)
  #module load mvapich2-2.3.6-gcc-11.1.0
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.76.0-mvapich2-2.3.6-gcc-11.2.0
  ifeq ($(PAPI), yes)
   PAPI_ROOT = $(LOCAL_INSTALL)/papi-6.0.0-mvapich2-2.3.6-gcc-11.2.0
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-11.2.0
  endif
 endif
  
 ifeq ($(COMPILERS), intel-intel)
  BOOST_ROOT = /usr/local/boost-1.66.0-intel-psxe-2018
  ifeq ($(PAPI), yes)
   PAPI_ROOT = 
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /usr/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /usr/local/qhull-2015.2-intel-psxe-2018
  endif
 endif
endif

ifeq ($(PLATFORM), quartz)

 ifeq ($(COMPILERS), intel-mvapich2)
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.76.0-mvapich2-2.3.6-intel-2021.2
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-intel-2021.2
  endif
  ifeq ($(PAPI), yes)
   PAPI_ROOT =
  endif
 endif

# ifeq ($(COMPILERS), intel-mvapich2)
#  #BOOST_ROOT = /usr/tce/packages/boost/boost-1.72.0-mvapich2-2.3-intel-19.0.4
#  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.75.0-mvapich2-2.3-intel-19.0.4
#  ifeq ($(EIGEN), yes)
#   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
#  endif
#  ifeq ($(QHULL), yes)
#   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-intel-19.0.4
#  endif
#  ifeq ($(PAPI), yes)
#   PAPI_ROOT =
#  endif
# endif

 ifeq ($(COMPILERS), intel-intel)
 #module swap mvapich2 impi
 # 1) intel/19.0.4   2) impi/2019.8   3) boost/1.75.0
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.75.0-impi-2019.8-intel-19.0.4
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-intel-19.0.4
  endif
  ifeq ($(PAPI), yes)
   PAPI_ROOT = 
  endif
 endif

 ifeq ($(COMPILERS), gcc-openmpi)
 #module swap intel gcc/4.9.3
 #module swap mvapich2 openmpi/4.1.0
 # 1) gcc/4.9.3   2) openmpi/4.1.0  3) boost/1.75.0
  BOOST_ROOT = $(LOCAL_INSTALL)/boost-1.75.0-openmpi-4.1.0-gcc-4.9.3
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-4.9.3
  endif
  ifeq ($(PAPI), yes)
   PAPI_ROOT = 
  endif
 endif

# ifeq ($(COMPILERS), gcc-mvapich2)
# #module swap intel gcc
# #boost-1.72.0-mvapich2-2.3-gcc-4.9.3
#  BOOST_ROOT = /usr/tce/packages/boost/boost-1.72.0-mvapich2-2.3-gcc-4.9.3
#  ifeq ($(EIGEN), yes)
#   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
#  endif
#  ifeq ($(QHULL), yes)
#   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-4.9.3
#  endif
#  ifeq ($(PAPI), yes)
#   PAPI_ROOT =
#  endif
# endif

 ifeq ($(COMPILERS), gcc-mvapich2)
 #module swap intel gcc/8.1.0
 #boost-1.72.0-mvapich2-2.3-gcc-8.1.0
  BOOST_ROOT = /usr/tce/packages/boost/boost-1.72.0-mvapich2-2.3-gcc-8.1.0
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = $(LOCAL_INSTALL)/eigen-3.3.9
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = $(LOCAL_INSTALL)/qhull-2020.2-gcc-8.1.0
  endif
  ifeq ($(PAPI), yes)
   PAPI_ROOT =
  endif
 endif

endif

ifeq ($(PLATFORM), centennial)
 ifeq ($(COMPILERS), intel-sgi)
#module swap compiler/intel compiler/intel/18.0.1.163
#module swap mpi/sgimpt mpi/sgimpt/2.17
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_mpt-2.17_intel-18.0.1.163
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-intel-18.0.1.163
  endif
 endif
endif

ifeq ($(PLATFORM), onyx)
 ifeq ($(COMPILERS), intel-cray)
#module swap PrgEnv-cray PrgEnv-intel/6.0.9
#intel/19.1.3.304
#cray-mpich/7.7.16 
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1-cray-mpich-7.7.16-PrgEnv-intel-6.0.9
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.9
   endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2020.2-PrgEnv-intel-6.0.9
   endif
 endif
 
 ifeq ($(COMPILERS), intel-cray-XXX)
#this one works, but slower.
#module swap PrgEnv-cray PrgEnv-intel/6.0.5
#automatically load cray-mpich/7.6.3
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_cray-mpich7.6.3_PrgEnv-intel6.0.5
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-intel6.0.5
  endif
 endif
 
 ifeq ($(COMPILERS), gcc-cray)
#module swap PrgEnv-cray PrgEnv-gnu/6.0.4
#automatically load cray-mpich/7.6.2
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_cray-mpich7.6.2_PrgEnv-gnu6.0.4
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-gnu6.0.4
  endif
 endif
 ifeq ($(COMPILERS), gcc-openmpi)
  BOOST_ROOT = 
  ifeq ($(PAPI), yes)
   PAPI_ROOT = 
  endif
 endif
endif

ifeq ($(PLATFORM), excalibur)
 ifeq ($(COMPILERS), intel-cray)
#PrgEnv-intel/5.2.82(default)
#module swap intel intel/18.0.1.163
#module swap cray-mpich cray-mpich/7.7.4
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_cray-mpich7.7.4_intel18.0.1.163
  ifeq ($(PAPI), yes)
   PAPI_ROOT = /p/home/yanb/local/papi-5.4.1_cray-mpich7.7.4_intel18.0.1.163
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-intel18.0.1.163
  endif
 endif
 ifeq ($(COMPILERS), gcc-cray)
#module swap PrgEnv-intel/5.2.82 PrgEnv-gnu/5.2.40
#module swap cray-mpich/7.2.4 cray-mpich/7.1.0
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_cray-mpich7.1.0_PrgEnv-gnu5.2.40
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-gnu5.2.40
  endif
 endif
 ifeq ($(COMPILERS), gcc-openmpi)
  BOOST_ROOT = /p/home/yanb/local/boost-1.57.0_openmpi-1.8.4_PrgEnv-gnu5.2.40
  ifeq ($(PAPI), yes)
   PAPI_ROOT = /p/home/yanb/local/papi-5.5.1_openmpi-1.8.4_PrgEnv-gnu5.2.40 
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-gnu5.2.40
  endif
 endif
endif

ifeq ($(PLATFORM), thunder)
 ifeq ($(COMPILERS), intel-sgi)
#module swap intel-compilers intel-compilers/17.0.2
#mpt/2.20(default)
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_mpt-2.20_intel-compilers-17.0.2
  ifeq ($(PAPI), yes)
   PAPI_ROOT = /p/home/yanb/local/papi-5.7.0_mpt-2.20_intel-compilers-17.0.2
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-intel-compilers-17.0.2
  endif
 endif
 ifeq ($(COMPILERS), gcc-sgi)
#module swap intel-compilers/2019_update1 gcc-compilers/7.3.0
#mpt/2.20(default)
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_mpt-2.20_gcc-compilers-7.3.0
  ifeq ($(PAPI), yes)
   PAPI_ROOT = /p/home/yanb/local/papi-5.7.0_mpt-2.20_gcc-compilers-7.3.0
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-gcc-compilers-7.3.0
  endif  
 endif
endif

ifeq ($(PLATFORM), topaz)
 ifeq ($(COMPILERS), intel-sgi)
  BOOST_ROOT = /p/home/yanb/local/boost-1.65.1_mpt-2.15_intel-18.0.1
  ifeq ($(PAPI), yes)
   PAPI_ROOT = /p/home/yanb/local/papi-5.4.1_mpt-2.15_intel-18.0.1
  endif
  ifeq ($(EIGEN), yes)
   EIGEN_ROOT = /p/home/yanb/local/eigen-3.3.4
  endif
  ifeq ($(QHULL), yes)
   QHULL_ROOT = /p/home/yanb/local/qhull-2015.2-intel-18.0.1
  endif
 endif
endif

ifeq ($(PLATFORM), lightning)
 ifeq ($(COMPILERS), intel-cray)
  #BOOST_ROOT = /home/yanb/local/boost-1.55.0_cray-mpich7.1.3_PrgEnv-intel5.2.40
  BOOST_ROOT = /home/yanb/local/boost-1.57.0_cray-mpich7.2.6_PrgEnv-intel5.2.82
 endif
endif

#########################################################
###         Part B: usually do not change             ###
#########################################################
###1. MPI wrapper, OpenMP and optimize/debug
# MPI wrapper
ifeq ($(COMPILERS), os_default)
 MPICXX = /usr/lib64/openmpi/bin/mpic++
endif

ifeq ($(COMPILERS), gcc-openmpi)
 MPICXX = mpic++
endif

ifeq ($(COMPILERS), intel-mvapich2)
 MPICXX = mpic++
endif

ifeq ($(COMPILERS), gcc-mvapich2)
 MPICXX = mpic++
endif

ifeq ($(COMPILERS), gcc-sgi)
 MPICXX = mpicxx
endif

ifeq ($(COMPILERS), gcc-cray)
 MPICXX = CC
endif

ifeq ($(COMPILERS), intel-intel)
 MPICXX = mpiicpc
endif

ifeq ($(COMPILERS), intel-sgi)
 MPICXX = mpicxx
endif

ifeq ($(COMPILERS), intel-cray)
 MPICXX = CC
endif

# OpenMP
ifeq ($(COMPILERS), os_default)
 OPENMP = -fopenmp
endif

ifeq ($(COMPILERS), gcc-openmpi)
 OPENMP = -fopenmp
endif

ifeq ($(COMPILERS), gcc-sgi)
 OPENMP = -fopenmp
endif

ifeq ($(COMPILERS), gcc-cray)
 OPENMP = -fopenmp
endif

ifeq ($(COMPILERS), intel-intel)
 OPENMP = -qopenmp -mt_mpi
endif

ifeq ($(COMPILERS), intel-sgi)
 OPENMP = -qopenmp -mt_mpi
endif

ifeq ($(COMPILERS), intel-cray)
 OPENMP = -qopenmp
endif

ifeq ($(COMPILERS), intel-mvapich2)
 OPENMP = -qopenmp
endif

###2 standard, optimize or debug
#C++11 or not?
C++11 = -std=c++11

#########################################
#optimization options
OPTIMIZE = -O3 -DNDEBUG $(C++11)
#gcc: full level of IEEE 754 compliance
#OPTIMIZE = -O3 -DNDEBUG -frounding-math -fsignaling-nans $(C++11)
#intel: -fp-model source also implies keyword precise; and it is nearly as twice slow as default -fp-model fast=1
#OPTIMIZE = -O3 -DNDEBUG -fp-model source $(C++11)   

#########################################
# debug options
# (a) for GCC
# 1. for debug with Boost, do not use -D_GLIBCXX_DEBUG because it builds into __debug namespace and cannot link Boost.
# 2. -fsanitize=bounds may require newer GCC like 6.3.0
#OPTIMIZE = -g -fsanitize=bounds -fstack-protector-all $(C++11)
#OPTIMIZE = -g -D_GLIBCXX_DEBUG $(C++11)
# (b) for Intel compilers
#OPTIMIZE = -O0 -g -traceback -debug all $(C++11)
#OPTIMIZE = -O0 -g -traceback -fstack-security-check -check-pointers=rw $(C++11)

# GCC quadmath, default off
#QUADMATH = -DQUADMATH -L/usr/local/gcc-4.6.2/lib64 -lquadmath

# CXXFLAGS
CXXFLAGS = $(OPTIMIZE) $(OPENMP) $(QUADMATH) $(MACRO_STRESS_STRAIN) $(MACRO_PRINT_CONTACT)

###3. configurations of BOOST, PAPI, Eigen and Qhull
ifeq ($(PLATFORM), soilblast)
  ifeq ($(COMPILERS), os_default)
   BOOST_LIB_DIR=-L/usr/lib64/openmpi/lib:/usr/lib64/compat-openmpi/lib:/usr/lib64
   BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_timer -lboost_chrono -lboost_system
   BOOST_RUN_LIB_DIR=-Wl,-rpath=/usr/lib64/openmpi/lib:/usr/lib64/compat-openmpi/lib:/usr/lib64
  else
   BOOST_INCLUDE=-I$(BOOST_ROOT)/include
   BOOST_LIB_DIR=-L$(BOOST_ROOT)/lib
   BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_timer -lboost_chrono -lboost_system
   BOOST_RUN_LIB_DIR=-Wl,-rpath=$(BOOST_ROOT)/lib
  endif

  ifeq ($(PAPI), yes)
   PAPI_INCLUDE=-I$(PAPI_ROOT)/include
   PAPI_LIB_DIR=-L$(PAPI_ROOT)/lib
   PAPI_LIBS=-lpapi
   PAPI_RUN_LIB_DIR=-Wl,-rpath=$(PAPI_ROOT)/lib
  endif

  ifeq ($(EIGEN), yes)
   EIGEN_INCLUDE=-I$(EIGEN_ROOT)
  endif

  ifeq ($(QHULL), yes)
   QHULL_INCLUDE=-I$(QHULL_ROOT)/src
   QHULL_LIB_DIR=-L$(QHULL_ROOT)/lib
   #QHULL_LIBS=-lqhullcpp -lqhullstatic_r
   QHULL_LIBS=-lqhullcpp -lqhull_r
   QHULL_RUN_LIB_DIR=-Wl,-rpath=$(QHULL_ROOT)/lib	
  endif
endif

ifeq ($(PLATFORM), micromorph)
  ifeq ($(COMPILERS), os_default)
   BOOST_LIB_DIR=-L/usr/lib64/openmpi/lib:/usr/lib64
   BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_timer -lboost_chrono -lboost_system
   BOOST_RUN_LIB_DIR=-Wl,-rpath=/usr/lib64/openmpi/lib:/usr/lib64
  else
   BOOST_INCLUDE=-I$(BOOST_ROOT)/include
   BOOST_LIB_DIR=-L$(BOOST_ROOT)/lib
   BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_timer -lboost_chrono -lboost_system
   BOOST_RUN_LIB_DIR=-Wl,-rpath=$(BOOST_ROOT)/lib
  endif
  
  ifeq ($(PAPI), yes)
   PAPI_INCLUDE=-I$(PAPI_ROOT)/include
   PAPI_LIB_DIR=-L$(PAPI_ROOT)/lib
   PAPI_LIBS=-lpapi
   PAPI_RUN_LIB_DIR=-Wl,-rpath=$(PAPI_ROOT)/lib
  endif

  ifeq ($(EIGEN), yes)
   EIGEN_INCLUDE=-I$(EIGEN_ROOT)
  endif

  ifeq ($(QHULL), yes)
   QHULL_INCLUDE=-I$(QHULL_ROOT)/src
   QHULL_LIB_DIR=-L$(QHULL_ROOT)/lib
   #QHULL_LIBS=-lqhullcpp -lqhullstatic_r
   QHULL_LIBS=-lqhullcpp -lqhull_r
   QHULL_RUN_LIB_DIR=-Wl,-rpath=$(QHULL_ROOT)/lib	
  endif
endif

# for supercomputers like Excalibur, Topaz, Thunder, Onyx, Centennial, Quartz, etc
BOOST_INCLUDE=-I$(BOOST_ROOT)/include
BOOST_LIB_DIR=-L$(BOOST_ROOT)/lib
BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_timer -lboost_chrono -lboost_system
BOOST_RUN_LIB_DIR=-Wl,-rpath=$(BOOST_ROOT)/lib

ifeq ($(PAPI), yes)
 PAPI_INCLUDE=-I$(PAPI_ROOT)/include
 PAPI_LIB_DIR=-L$(PAPI_ROOT)/lib
 PAPI_LIBS=-lpapi
 PAPI_RUN_LIB_DIR=-Wl,-rpath=$(PAPI_ROOT)/lib
endif

ifeq ($(EIGEN), yes)
 EIGEN_INCLUDE=-I$(EIGEN_ROOT)
endif

ifeq ($(QHULL), yes)
 QHULL_INCLUDE=-I$(QHULL_ROOT)/src
 QHULL_LIB_DIR=-L$(QHULL_ROOT)/lib
 ## it depends on the type of Boost libraries; or use -Bstatic -Bdynamic to control.
 QHULL_LIBS=-lqhullcpp -lqhullstatic_r
 #QHULL_LIBS=-lqhullcpp -lqhull_r
 #QHULL_RUN_LIB_DIR=-Wl,-rpath=$(QHULL_ROOT)/lib
endif

INCLUDE=$(BOOST_INCLUDE) $(PAPI_INCLUDE) $(EIGEN_INCLUDE) $(QHULL_INCLUDE)
LIB_DIR=$(BOOST_LIB_DIR) $(PAPI_LIB_DIR) $(QHULL_LIB_DIR)
LIBS=$(BOOST_LIBS) $(PAPI_LIBS) $(QHULL_LIBS)
RUN_LIB_DIR=$(BOOST_RUN_LIB_DIR) $(PAPI_RUN_LIB_DIR) $(QHULL_RUN_LIB_DIR)

###4. makefile
SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
#OBJECTS = $(patsubst %.cpp, %.o, $(SOURCES))
EXECUTABLE = paraEllip3d

.PHONY: all tar clean

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(MPICXX) -o $@ $(CXXFLAGS) $(OBJECTS) $(LIB_DIR) $(LIBS) $(RUN_LIB_DIR)

%.o: %.cpp
	$(MPICXX) -c $< -o $@ $(CXXFLAGS) $(INCLUDE)

sinclude $(SOURCES:.cpp=.d)

%.d: %.cpp
	$(MPICXX) -MM $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

tar:
	tar -cvf $(EXECUTABLE).tar *.h *.cpp makefile* readme

clean:
	-rm -f *.o *.d  *~ *.tar $(EXECUTABLE)

# sinclude is always resolved even if make tar/clean
```
## Running on supercomputers 
Note that the execution environment must conform with the compile environment.
This is done through variables that are defined in the job script.

Here are job script examples for a 2-node MPI execution on LLNL Quartz.
Note that `./paraEllip3d` needs to be the version that conforms with the compile 
environment.

### OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3)
The job script `run.quartz.gcc.openmpi.sh` is as follows:

```
#!/bin/bash
## for Slurm
#SBATCH -N 2
#SBATCH -J simuType101
#SBATCH -t 24:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=someone@somewhere.org
#SBATCH -A uco

## load modules
module swap intel gcc/4.9.3
module swap mvapich2 openmpi/4.0.0

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-openmpi-4.0.0-gcc-4.9.3/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-gcc-4.9.3/lib:$LD_LIBRARY_PATH

srun -N2 -n72 ./paraEllip3d input.txt
```
To submit the job:
```console
$ sbatch run.quartz.gcc.openmpi.sh
```

### Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4)
The job script `run.quartz.intel.impi.sh` is as follows:
```
#!/bin/bash
## for Slurm
#SBATCH -N 2
#SBATCH -J simuType101
#SBATCH -t 24:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=someone@somewhere.org
#SBATCH -A uco

## load modules
module swap mvapich2 impi/2019.8

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-19.0.4/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-impi-2019.8-intel-19.0.4/lib:$LD_LIBRARY_PATH

srun -N2 -n72 ./paraEllip3d input.txt
```

To submit the job:
```console
$ sbatch run.quartz.intel.impi.sh
```

### Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2)
The job script `run.quartz.intel.mvapich2.sh` is as follows:
```
#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simuType101
#SBATCH -t 24:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address
#SBATCH -A uco

## load modules
#by default

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-2021.2/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.76.0-mvapich2-2.3.6-intel-2021.2/lib:$LD_LIBRARY_PATH

srun -N1 -n36 ./paraEllip3d input.txt

```

To submit the job:
```console
$ sbatch run.quartz.intel.mvapich2.sh
```

## Debugging on supercomputers 
### Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2) with Valgrind
In `makefile`, comment this line
```
OPTIMIZE = -O3 -DNDEBUG $(C++11)
```
and uncomment this line
```
OPTIMIZE = -O0 -g -traceback -debug all $(C++11)
```
or
```
OPTIMIZE = -O0 -g -traceback -fstack-security-check -check-pointers=rw $(C++11)
```
then `make`, it will build executable for debugging.

The job script `run.quartz.intel.mvapich2.debug.sh` using valgrind may look like this:
```
#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simuType101
#SBATCH -t 01:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address
#SBATCH -A uco

## load modules
module swap intel/19.0.4 intel/2021.2

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-2021.2/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.76.0-mvapich2-2.3.6-intel-2021.2/lib:$LD_LIBRARY_PATH

srun -N1 -n36 valgrind --tool=memcheck --leak-check=full --track-origins=yes --error-limit=no --log-file=vglog.%p ./paraEllip3d input.txt


```

### Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4) for GDB
In `makefile`, comment this line
```
OPTIMIZE = -O3 -DNDEBUG $(C++11)
```
and uncomment this line
```
OPTIMIZE = -O0 -g -traceback -debug all $(C++11)
```
or
```
OPTIMIZE = -O0 -g -traceback -fstack-security-check -check-pointers=rw $(C++11)
```
then `make`, it will build executable for debugging.

The job script `run.quartz.intel.impi.debug.sh` may look like this:
```
#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simu
#SBATCH -t 01:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address
#SBATCH -A uco

## load modules
module swap mvapich2 impi/2019.8

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-19.0.4/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-impi-2019.8-intel-19.0.4/lib:$LD_LIBRARY_PATH

srun -N1 -n36 ./paraEllip3d input.txt

```

or it can be edited for serial computing:
```
#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simu
#SBATCH -t 01:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address
#SBATCH -A uco

## load modules
module swap mvapich2 impi/2019.8

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-19.0.4/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-impi-2019.8-intel-19.0.4/lib:$LD_LIBRARY_PATH

srun -N1 -n1 ./paraEllip3d input.txt

```

Note the parameters in input.txt must be changed accordingly for serial computing:
```
mpiProcX  1
mpiProcY  1
mpiProcZ  1
```

To debug using GDB, run the following command:
```
gdb ./paraEllip3d corefile
```

## Performance on supercomputers 
### Parallel file systems
`ParaEllip3d-cfd` uses parallel IO heavily and it might be file systems sensitive. It is recommended to run 
`ParaEllip3d-cfd` on parallel file systems like Lustre to achieve the best performance. It could be running 
5x or more times slower than on none-parallel file systems.

On `quartz.lln.gov`, it is recommended to not run it on home directory, /usr/workspace, /tmp or usr/tmp, 
instead, run it on /p/lustre#, /p/gpfs# or /p/gscratch#. The file systems are described in the page:
https://hpc.llnl.gov/training/tutorials/livermore-computing-resources-and-environment#temp-file-systems

### Various compilers and MPI libraries
Three combinations of compilers and MPI libraries are tested on Quartz's Lustre parallel file systems 
using gravitational deposition simulations of 5,000 5mm particles. The input data and Slurm job script 
are listed in this page:
https://gitlab.com/micromorph/paraellip3d-cfd/-/tree/main/examples/simuType101-deposit/long_runtime

The MPI_Wtime of the three combinations are listed here:
* OpenMPI (openmpi-4.0.0) and GCC compilers (gcc-4.9.3):        MPI_Wtime 5.638294e+04 seconds (15.7 hours)
* Intel MPI (impi-2019.8) and Intel compilers (intel-19.0.4):   MPI_Wtime 3.178087e+04 seconds (8.83 hours)
* Mvapich2 (mvapich2-2.3.6) and Intel compilers (intel-2021.2): MPI_Wtime 3.157400e+04 seconds (8.77 hours)


## Building and running on local workstations/desktops/laptops
We also demonstrate the building process of ParaEllip3d-CFD on a local workstation (micromorph.colorado.edu), 
which runs the latest Fedora 34. There are two options, one using OS-default packages, and the other building 
compilers and MPI libraries for management by environment modules.

Note that the Micromorph workstation is deployed with Torque and Slurm, two different resource managers. 
The two resource managers cannot co-exist, and Slurm is chosen as the default. 

### Using Slurm (default)

#### Use compilers and MPI libraries managed by environment modules.

For a well-managed workstation with environment modules and Slurm resource manager configured,
the building and running procedures are the same as those on supercomputers. The process of
installing compilers, MPI libraries, environment modules, Slurm, and the four libraries is 
not covered here. Note it is important to install pmix for Slurm: 
```
sudo dnf install pmix pmix-devel
```
and configure OpenMPI like this:
```
./configure --prefix=/usr/local/openmpi-4.1.1-gcc-11.2.0 --with-pmix=external
```
and configure Mvapich2 like this:
```
./configure --prefix=/usr/local/mvapich2-2.3.6-gcc-11.2.0  --with-pmi=pmix --with-pm=slurm
```

##### OpenMPI (openmpi-4.1.1) and GCC compilers (gcc-11.2.0)
1\. Load module environment:
```console
$ module load openmpi-4.1.1-gcc-11.2.0 
```

2\. Edit `makefile` according to your environment:
```
PLATFORM = micromorph
COMPILERS = gcc-openmpi
LOCAL_INSTALL=/usr/local
```

then:
```
make
```

To run a job, the script `run.fedora.sh` for Slurm can be like this:
```
#!/bin/sh

#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address

#SBATCH -J run
#SBATCH -N 1
#SBATCH --ntasks-per-node=64
#SBATCH -t 24:00:00
#SBATCH -p batch

## load modules
module load openmpi-4.1.1-gcc-11.2.0

# export additional environment variables
export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH


## cd to work dir
cd $SLURM_SUBMIT_DIR
srun --mpi=pmix -N1 -n64 ./paraEllip3d input.txt
```

##### Mvapich2 (mvapich2-2.3.6) and GCC compilers (gcc-11.2.0)
1\. Load module environment:
```console
$ module load mvapich2-2.3.6-gcc-11.2.0 
```

2\. Edit `makefile` according to your environment:
```
PLATFORM = micromorph
COMPILERS = gcc-mvapich2
LOCAL_INSTALL=/usr/local
```

then:
```
make
```

To run a job, the script `run.fedora.sh` for Slurm can be like this:
```
#!/bin/sh

#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address

#SBATCH -J run
#SBATCH -N 1
#SBATCH --ntasks-per-node=64
#SBATCH -t 24:00:00
#SBATCH -p batch

## load modules
module load mvapich2-2.3.6-gcc-11.2.0

# export additional environment variables
export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-mvapich2-2.3.6-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH


## cd to work dir
cd $SLURM_SUBMIT_DIR
srun --mpi=pmix -N1 -n64 ./paraEllip3d input.txt
```

### Using Torque
#### Use compilers and MPI libraries managed by environment modules.

For a well-managed workstation with environment modules and Torque resource manager available,
the building and running procedures are the same as those on supercomputers. The process of
installing compilers, MPI libraries, environment modules, Torque, and the four libraries is 
not covered here. Note it is important to configure OpenMPI like this:
```
./configure --prefix=/usr/local/openmpi-4.1.1-gcc-11.2.0 --with-tm=/var/lib/torque
```

##### OpenMPI (openmpi-4.1.1) and GCC compilers (gcc-11.2.0)
1\. Load module environment:
```console
$ module load openmpi-4.1.1-gcc-11.2.0 
```

2\. Edit `makefile` according to your environment:
```
PLATFORM = micromorph
COMPILERS = gcc-openmpi
LOCAL_INSTALL=/usr/local
```

then:
```
make
```

To run a job, the script `run.fedora.sh` for Torque can be like this:
```
#!/bin/sh

#PBS -m abe
#PBS -M your_email_address
#PBS -j oe

#PBS -N run
#PBS -l nodes=1:ppn=64
#PBS -l walltime=24:00:00

module load openmpi-4.1.1-gcc-11.2.0

export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH

cd $PBS_O_WORKDIR
mpirun -np 64 ./paraEllip3d input.txt
```

##### Mvapich2 (mvapich2-2.3.6) and GCC compilers (gcc-11.2.0)
1\. Load module environment:
```console
$ module load mvapich2-2.3.6-gcc-11.2.0 
```

2\. Edit `makefile` according to your environment:
```
PLATFORM = micromorph
COMPILERS = gcc-mvapich2
LOCAL_INSTALL=/usr/local
```

then:
```
make
```

To run a job, the script `run.fedora.sh` for Torque can be like this:
```
#!/bin/sh

#PBS -m abe
#PBS -M your_email_address
#PBS -j oe

#PBS -N run
#PBS -l nodes=1:ppn=64
#PBS -l walltime=24:00:00

module load mvapich2-2.3.6-gcc-11.2.0

export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-mvapich2-2.3.6-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH

cd $PBS_O_WORKDIR
mpirun_rsh -np 64 ./paraEllip3d input.txt
```

### Using OS-default software packages
ParaEllip3d can be run on Linux workstations/desktops/laptops without the compilers and MPI 
libraries managed by environment modules, which is easy for personal use. Below are the steps
to run it on Fedora Linux 34.

First, install the following packages on Fedora 34 (no need to build your own OpenMPI and Boost C++):
```console
# dnf install gcc-c++
# dnf install openmpi openmpi-devel
# dnf install boost-openmpi boost-openmpi-devel

$ which g++
/usr/bin/g++
$ g++ --version
g++ (GCC) 11.1.1 20210531 (Red Hat 11.1.1-3)

$ export PATH=/usr/lib64/openmpi/bin/:$PATH
$ which mpic++
/usr/lib64/openmpi/bin/mpic++
$ mpic++ --version
g++ (GCC) 11.1.1 20210531 (Red Hat 11.1.1-3)
```

and build Qhull at `/usr/local/qhull-2020.2-gcc-11.1.1/`:
```
$ wget http://www.qhull.org/download/qhull-2020-src-8.0.2.tgz
$ tar xzf qhull-2020-src-8.0.2.tgz
$ cp -pr qhull-2020.2/ qhull-2020.2-gcc-11.1.1
$ cd qhull-2020.2-gcc-11.1.1/
$ make
$ cd ..
$ sudo mv qhull-2020.2-gcc-11.1.1/ /usr/local
```
and extract eigen-3.3.9 at `/usr/local/eigen-3.3.9/`:
```
$ wget https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.tar.gz
$ tar xzf eigen-3.3.9.tar.gz
$ sudo cp -pr eigen-3.3.9 /usr/local
```

Then select the following options in `paraellip3d-cfd/src/makefile`:
```
PLATFORM = micromorph
COMPILERS = os_default
LOCAL_INSTALL=/usr/local
```

and type the command
```
$ make
```
to compile, and it will generate the executable file `paraEllip3d`.

To start a simulation, the following command can be issued:
```
$ /usr/lib64/openmpi/bin/mpirun -np 64 --oversubscribe ./paraEllip3d input.txt
```

Or, the following script can be used to submit a job under Torque:
```
#!/bin/sh

#PBS -m abe
#PBS -M your_email_address
#PBS -j oe

#PBS -N run
#PBS -l nodes=1:ppn=64
#PBS -l walltime=24:00:00

export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.1.1/lib:$LD_LIBRARY_PATH

cd $PBS_O_WORKDIR
/usr/lib64/openmpi/bin/mpirun -np 64 --oversubscribe ./paraEllip3d input.txt
```

## Debugging on local workstations/desktops/laptops
### OpenMPI (openmpi-4.1.1) and GCC compilers (gcc-11.2.0)
In `makefile`, comment this line
```
OPTIMIZE = -O3 -DNDEBUG $(C++11)
```
and uncomment this line
```
#OPTIMIZE = -g -fsanitize=bounds -fstack-protector-all $(C++11)
```
then `make`, it will build executable for debugging.

The job script `rrun.fedora.debug.sh` using valgrind may look like this:
```
#!/bin/sh

#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address

#SBATCH -J debug
#SBATCH -N 1
#SBATCH --ntasks-per-node=64
#SBATCH -t 1:00:00
#SBATCH -p batch

## load modules
module load openmpi-4.1.1-gcc-11.2.0

# export additional environment variables
export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH


## cd to work dir
cd $SLURM_SUBMIT_DIR
srun --mpi=pmix -N1 -n64 valgrind --tool=memcheck --leak-check=full --track-origins=yes --error-limit=no --log-file=vglog.%p ./paraEllip3d input.txt
```

or it can be edited for serial computing:
```
#!/bin/sh

#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email_address

#SBATCH -J debug
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH -t 1:00:00
#SBATCH -p batch

## load modules
module load openmpi-4.1.1-gcc-11.2.0

# export additional environment variables
export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH


## cd to work dir
cd $SLURM_SUBMIT_DIR
srun --mpi=pmix -N1 -n1 valgrind --tool=memcheck --leak-check=full --track-origins=yes --error-limit=no --log-file=vglog.%p ./paraEllip3d input.txt

```

Note the parameters in input.txt must be changed accordingly for serial computing:
```
mpiProcX  1
mpiProcY  1
mpiProcZ  1
```

To debug using GDB, run the following command:
```
gdb ./paraEllip3d corefile
```


## Example input files
ParaEllip3d supports many types of simulation. Here are a few examples with input files. 
Refer to the directory of paraellip3d-cfd/example for more types.

### simuType101-deposit: an example of pure DEM simulation of gravitational deposition.
```
###################################################################                                         
# Below are 25 generic parameters required by every simulation                                              
###################################################################                                         

#simulation type
#001 - proceed simulation with preset state
#002 - tune up mass percentage from number percentage
#003 - trim particles to a certain space domain
#004 - remove particles inside a spherical domain
#005 - calculate mass percentage from particle file
#101 - deposit spatially scattered particles into a rigid container
#102 - resume deposition using specified data file of particles and boundaries
#201 - isotropic 1, low confining pressure
#202 - isotropic 2, increasing confining pressure
#203 - isotropic 3, loading-unloading-reloading
#301 - oedometer 1
#302 - oedometer 2, loading-unloading-reloading
#401 - triaxial 1
#402 - triaxial 2, loading-unloading-reloading
#411 - plane strain 1, in x direction
#412 - plane strain 2, loading-unloading-reloading
#501 - true triaxial 1, create confining stress state
#502 - true triaxial 2, increase stress in one direction
#601 - expand particles volume inside a virtual cavity
#602 - resume simulation after expanding particles volume inside a virtual cavity
#701 - couple with supersonic air flow, bottom "left" part, R-H conditions
#702 - couple with supersonic air flow, bottom "left" part
#703 - couple with supersonic air flow, rectangular "left" part
#704 - couple with supersonic air flow, spherical "left" part
#705 - couple with supersonic air flow, rectangular "left" part, and a zone below
#801 - pure supersonic air flow, bottom "left" part, R-H conditions
#802 - pure supersonic air flow, bottom "left" part
#803 - pure supersonic air flow, rectangular "left" part
#804 - pure supersonic air flow, spherical "left" part
#805 - pure supersonic air flow, rectangular "left" part, and a zone below
simuType  101

#grids/processes in x, y, z directions for MPI parallelization, which
#is recommended for more than thousands of particles. By default these
#values are 1 for serial computing. They must satisfy equation:
#mpiProcX * mpiProcY * mpiProcZ = np, where np is the number of processes
#specified in mpirun command line.
mpiProcX  3
mpiProcY  3
mpiProcZ  4

#threads per process for OpenMP parallelization, which is recommended 
#for more than thousands of particles. By default this value is 1 for 
#single-thread computing. If it > 1, multiple-threads computing is 
#invoked.
ompThreads  1

#starting time step, must >= 1
startStep  1

#ending time step, must >= 1
endStep  1.2E+6

#starting snapshot number, (endsStep - startStep +1) must be divided by (endSnap - startSnap + 1)
startSnap  1

#ending snapshot number
endSnap  100

#time accrued prior to computation
timeAccrued  0

#time step size
timeStep  5.0E-7

#coefficient of mass scaling
massScale  1.0

#coefficient of moment scaling
mntScale  1.0

#coefficient of gravity scaling
gravScale  1.0

#gravitational acceleration
gravAccel  9.8

#damping ratio of background damping of force
forceDamp  0

#damping ratio of background damping of moment
momentDamp  0

#damping ratio of inter-particle contact
contactDamp  0.85

#coefficient of inter-particle static friction
contactFric  0.5

#coefficient of particle-boundary static friction
boundaryFric  0.5

#coefficient of inter-particle cohesion
#5.0e+8; cohesion between particles (10kPa)
contactCohesion  0

#particle Young's modulus
#quartz sand E = 45GPa
young  2.9E+10

#particle Poisson's ratio
#quartz sand v = 0.25
poisson  0.25

#particle specific gravity
#quartz sand Gs = 2.65
specificG  2.65

#maximum relative overlap
maxRelaOverlap  1.0E-3

###################################################################
# Below are additional parameters for simulation type 101
###################################################################

#container coordinates
#minX minY minZ maxX maxY maxZ
minX  0
minY  0
minZ  0
maxX  0.04
maxY  0.04
maxZ  0.64

#trimming height
#coordinate in +Z direction to trim the particle assembly after deposition
trimHeight  0.0425

#particle layers
#0-one particle; 1-one layer of particles; 2-multiple layers of particles (default)
particleLayers  2

#particle aspect ratio
#ratio of radius b to radius a
ratioBA  0.8
#ratio of radius c to radius a
ratioCA  0.6

#particle gradation curve
#number of sieves
sieveNum  5
#[number-percentage-smaller, particle-size (half)]
1.00  2.50e-3
0.80  2.30e-3
0.60  2.00e-3
0.30  1.50e-3
0.10  1.00e-3

#particle box size (half)
ptclBoxSize  2.50e-3

#bottom gap
bottomGap  1

#side gap
sideGap  1

#layer gap
layerGap  0

#horizontal generation mode: 
#0-from side to side
#1-xy symmetric, and from center outward
#2-virtual meshing method for a wide range of particle size (default)
genMode  0

# particle boundary tracking and grid updating mode
#-1 - update with boundaries. All others update with particles.
# 0 - update in +Z
# 1 - update in +X, -X, +Y, -Y, +Z, but +X, -X, +Y, -Y are preset for limited explosion range
# 2 - update in +X, -X, +Y, -Y, +Z
# 3 - update in +X, -X, +Y, -Y, +Z, -Z
# default 0
gridUpdate  0

# drag coefficient
Cd  0

# ambient fluid density
fluidDensity  1.225

```


### simuType401-triaxial-1: an example of laboratory triaxial compression test.
```
###################################################################                                         
# Below are 25 generic parameters required by every simulation                                              
###################################################################                                         

#simulation type
#001 - proceed simulation with preset state
#002 - tune up mass percentage from number percentage
#003 - trim particles to a certain space domain
#004 - remove particles inside a spherical domain
#005 - calculate mass percentage from particle file
#101 - deposit spatially scattered particles into a rigid container
#102 - resume deposition using specified data file of particles and boundaries
#201 - isotropic 1, low confining pressure
#202 - isotropic 2, increasing confining pressure
#203 - isotropic 3, loading-unloading-reloading
#301 - oedometer 1
#302 - oedometer 2, loading-unloading-reloading
#401 - triaxial 1
#402 - triaxial 2, loading-unloading-reloading
#411 - plane strain 1, in x direction
#412 - plane strain 2, loading-unloading-reloading
#501 - true triaxial 1, create confining stress state
#502 - true triaxial 2, increase stress in one direction
#601 - expand particles volume inside a virtual cavity
#602 - resume simulation after expanding particles volume inside a virtual cavity
#701 - couple with supersonic air flow, bottom "left" part, R-H conditions
#702 - couple with supersonic air flow, bottom "left" part
#703 - couple with supersonic air flow, rectangular "left" part
#704 - couple with supersonic air flow, spherical "left" part
#705 - couple with supersonic air flow, rectangular "left" part, and a zone below
#801 - pure supersonic air flow, bottom "left" part, R-H conditions
#802 - pure supersonic air flow, bottom "left" part
#803 - pure supersonic air flow, rectangular "left" part
#804 - pure supersonic air flow, spherical "left" part
#805 - pure supersonic air flow, rectangular "left" part, and a zone below
simuType  401

#grids/processes in x, y, z directions for MPI parallelization, which
#is recommended for more than thousands of particles. By default these
#values are 1 for serial computing. They must satisfy equation:
#mpiProcX * mpiProcY * mpiProcZ = np, where np is the number of processes
#specified in mpirun command line.
mpiProcX  3
mpiProcY  3
mpiProcZ  4

#threads per process for OpenMP parallelization, which is recommended 
#for more than thousands of particles. By default this value is 1 for 
#single-thread computing. If it > 1, multiple-threads computing is 
#invoked.
ompThreads  1

#starting time step, must >= 1
startStep  1

#ending time step, must >= 1
endStep  1.0E+5

#starting snapshot number, (endsStep - startStep +1) must be divided by (endSnap - startSnap + 1)
startSnap  1

#ending snapshot number
endSnap  100

#time accrued prior to computation
timeAccrued  0

#time step size
timeStep  5.0E-6

#coefficient of mass scaling
massScale  75

#coefficient of moment scaling
mntScale  75

#coefficient of gravity scaling
gravScale  0

#gravitational acceleration
gravAccel  9.8

#damping ratio of background damping of force
forceDamp  0

#damping ratio of background damping of moment
momentDamp  0

#damping ratio of inter-particle contact
contactDamp  1

#coefficient of inter-particle static friction
contactFric  0.5

#coefficient of particle-boundary static friction
boundaryFric  0.5

#coefficient of inter-particle cohesion
#5.0e+8; cohesion between particles (10kPa)
contactCohesion  0

#particle Young's modulus
#quartz sand E = 45GPa
young  2.9E+10

#particle Poisson's ratio
#quartz sand v = 0.25
poisson  0.25

#particle specific gravity
#quartz sand Gs = 2.65
specificG  2.65

#maximum relative overlap
maxRelaOverlap  1.0E-2

###################################################################
# Below are additional parameters for simulation type 401
###################################################################

# file name of boundary file
boundaryFile  ini_boundary

# file name of particle file
particleFile  ini_particle

# initialize particles state (velocity, omga, force, moment) from data file or not?
toInitParticle  0

# particle boundary tracking and grid updating mode
#-1 - update with boundaries. All others update with particles.
# 0 - update in +Z
# 1 - update in +X, -X, +Y, -Y, +Z, but +X, -X, +Y, -Y are preset for limited explosion range
# 2 - update in +X, -X, +Y, -Y, +Z
# 3 - update in +X, -X, +Y, -Y, +Z, -Z
# default 0
gridUpdate  -1

# triaxial types
# 1 - constant confining
# 2 - conduct loading-unloading-reloading path
triaxialType  1

# constant confining pressure
sigmaConf  1.0E+5

# traction error tolerance on boundary equilibrium
tractionErrorTol  0.01

# boundary rate, a fictitious number
boundaryRate  6.0E-3

```

### simuType701: couple with supersonic air flow, bottom "left" part, R-H conditions, an example of DEM-CFD coupled simulation.
```
###################################################################
# Below are 25 generic parameters required by every simulation
###################################################################

#simulation type
#001 - proceed simulation with preset state
#002 - tune up mass percentage from number percentage
#003 - trim particles to a certain space domain
#004 - remove particles inside a spherical domain
#005 - calculate mass percentage from particle file
#101 - deposit spatially scattered particles into a rigid container
#102 - resume deposition using specified data file of particles and boundaries
#201 - isotropic 1, low confining pressure
#202 - isotropic 2, increasing confining pressure
#203 - isotropic 3, loading-unloading-reloading
#301 - oedometer 1
#302 - oedometer 2, loading-unloading-reloading
#401 - triaxial 1
#402 - triaxial 2, loading-unloading-reloading
#411 - plane strain 1, in x direction
#412 - plane strain 2, loading-unloading-reloading
#501 - true triaxial 1, create confining stress state
#502 - true triaxial 2, increase stress in one direction
#601 - expand particles volume inside a virtual cavity
#602 - resume simulation after expanding particles volume inside a virtual cavity
#701 - couple with supersonic air flow, bottom "left" part, R-H conditions
#702 - couple with supersonic air flow, bottom "left" part
#703 - couple with supersonic air flow, rectangular "left" part
#704 - couple with supersonic air flow, spherical "left" part
#705 - couple with supersonic air flow, rectangular "left" part, and a zone below
#801 - pure supersonic air flow, bottom "left" part, R-H conditions
#802 - pure supersonic air flow, bottom "left" part
#803 - pure supersonic air flow, rectangular "left" part
#804 - pure supersonic air flow, spherical "left" part
#805 - pure supersonic air flow, rectangular "left" part, and a zone below
simuType  701

#grids/processes in x, y, z directions for MPI parallelization, which
#is recommended for more than thousands of particles. By default these
#values are 1 for serial computing. They must satisfy equation:
#mpiProcX * mpiProcY * mpiProcZ = np, where np is the number of processes
#specified in mpirun command line.
mpiProcX  2
mpiProcY  2
mpiProcZ  3

#threads per process for OpenMP parallelization, which is recommended 
#for more than thousands of particles. By default this value is 1 for 
#single-thread computing. If it > 1, multiple-threads computing is 
#invoked.
ompThreads  1

#starting time step, must >= 1
startStep  1

#ending time step, must >= 1
endStep  2500

#starting snapshot number, (endsStep - startStep +1) must be divided by (endSnap - startSnap + 1)
startSnap  1

#ending snapshot number
endSnap  100

#time accrued prior to computation
timeAccrued  0

#time step size
timeStep  5.0E-7

#coefficient of mass scaling
massScale  1

#coefficient of moment scaling
mntScale  1

#coefficient of gravity scaling
gravScale  1

#gravitational acceleration
gravAccel  9.8

#damping ratio of background damping of force
#for dynamic simu it is 0
#for quasi-static simu using DR, it is fictitous
forceDamp  0

#damping ratio of background damping of moment
momentDamp  0

#damping ratio of inter-particle contact
contactDamp  0.55

#coefficient of inter-particle static friction
contactFric  0.5

#coefficient of particle-boundary static friction
boundaryFric  0.5

#coefficient of inter-particle cohesion
#5.0e+8; cohesion between particles (10kPa)
contactCohesion  0

#particle Young's modulus
#quartz sand E = 45GPa
young  4.5E+10

#particle Poisson's ratio
#quartz sand v = 0.25
poisson  0.25

#particle specific gravity
#quartz sand Gs = 2.65
specificG  2.65

#maximum relative overlap
maxRelaOverlap  1.0E-2

###################################################################
# Below are additional parameters for simulation type 701
###################################################################

# file name of the input boundary file
boundaryFile  ini_boundary

# file name of the input particle file
particleFile  ini_particle

# initialize particles state (velocity, omga, force, moment) from data file or not?
toInitParticle  0

# particle grid estimate
ptclGrid  4

# drag coefficient
Cd  0.9

# porosity of particles
porosity  0.45

# drag coefficient inside porous media, fictitous
Cdi  10

# time integration schemes: 1-Euler, 2-RK2, 3-RK3
# default 1
RK  1

# Riemann solver
#  0: Roe solver
#  1: HLLC solver
#  2: HLLE solver
#  3: Roe + HLLC (for negative phenomenon)
#  4: Roe + HLLE (for negative phenomenon)
#  5: Roe + exact (for negative phenomenon)
# -1: exact solver
# -2: Lax-Friedrichs scheme
# -3: Lax-Wendroff scheme (two-step Richtmyer version)
solver  -1

# CourantFriedrichsLewy condition
CFL  0.5

# ratio of specific heat capacity of air
airGamma  1.4

# "right" density
rightDensity  1.225

# "right" pressure
rightPressure  1.01325E+5

# "right" velocity
rightVelocity  0

# "left" part types
# 1 - bottom "left" part, R-H conditions
# 2 - bottom "left" part
# 3 - rectangular "left" part
# 4 - spherical "left" part
leftType  1

# fluid domain
x1F  0.0
y1F  0.0
z1F  0.0
x2F  1.0E-1
y2F  1.0E-1
z2F  5.0E-1

# boundary condition for x1, x2, y1, y2, z1, z2 directions respectively
# 0: non-reflecting
# 1: reflecting
x1Reflecting  0
x2Reflecting  0
y1Reflecting  0
y2Reflecting  0
z1Reflecting  0
z2Reflecting  0

#upper boundary z2L of "left" part
#must satisfy z2L > z1, z2L <= z2
#assume z1L==z1
z2L  5.0E-2

# shock Mach number
shockMach  5

# print more particles
printPtclNum  2
# list of particle IDs
1
12
```

## Post-processing and visualization
There are many executables, scripts and macros that can be used for post-processing 
the output of ParaEllip3d-CFD toward visualization. They are located at these sub-directories: plot, 
script, matlab and macro.

### Compiling
We use our local workstation, `micromorph.colorado.edu`, as an example to build the plot tools 
and copy them to `$HOME/bin` for use:

```console
$ cd plot
$ make
$ make unique
$ cp -pr plot3 plotvec plotContact plotContactAll dupSpecimen uniqueContact calcStressAll $HOME/bin

$ cd plot3bin
$ ./compile.sh
$ cp -pr plot3bin $HOME/bin

$ cd ../plotBdryContact
$ ./compile.sh
$ cp -pr plotBdryContact $HOME/bin

$ cd ../sortParaIJK
$ ./compile.sh
$ cp -pr sortParaIJK $HOME/bin

$ cd ../plot_tetra_contact
$ ./compile.sh
$ cp -pr plot_tetra_contact $HOME/bin

$ cd ../plot_tetra_particle
$ ./compile.sh
$ cp -pr plot_tetra_particle plot_tetra_particle_with_variable $HOME/bin

$ cd ../plot_tetra_particle_filter_contact
$ ./compile.sh
$ cp -pr plot_tetra_particle_filter_contact $HOME/bin

$ cd ../plot_tetra_particle_filter_distance
$ ./compile.sh
$ cp -pr  plot_tetra_particle_filter_distance $HOME/bin
```

The DEM particle visualization is based on Tecplot binary data for best performance, 
which are also accepted by ParaView and Visit.

### For DEM
#### To visualize particles
Typically, three steps are needed to render the DEM particles from the output of 
ParaEllip3d-CFD execution:

1. plot3: convert ellipsoid to surface quadrilateral elements.
1. preplot (a free Tecplot tool): convert text data to binary.
1. tecplot: read binary data and visualize.

Many scripts are provided to facilitate post-processing, for example:
```console
$ plot3
-- Plot All Particles --
Usage:
1) process a single file:  plot particle_file
   --example: plot iso_particle_008
2) process multiple files: plot particle_file_prefix  first_suffix  last_suffix  suffix_increment
   --example: plot iso_particle  0  100  5
   
$ myptcl
purpose: convert ellipsoid to Tecplot format
  usage: myptcl prefix start_snap end_snap increment
example: myptcl deposit 1 100 5

$ myptclbin
purpose: convert ellipsoid to Tecplot format
  usage: myptclbin prefix start_snap end_snap increment
example: myptclbin deposit 1 100 5
   
$ myptcl.para
purpose: convert ellipsoid to Tecplot format in parallel
  usage: myptcl.para prefix start_snap end_snap increment number_of_process
example: myptcl.para deposit 1 100 5 10

$ myptclbin.para
purpose: convert ellipsoid to Tecplot format in parallel
  usage: myptclbin.para prefix start_snap end_snap increment number_of_process
example: myptclbin.para deposit 1 100 5 10
```

As an example, the post-processing for the simulation example 
`simuType101-deposit/long_runtime/intel-impi-1-nodes` 
is done through the following steps:
```
$ export PATH=/usr/local/tecplot/360ex_2021r1/bin:$PATH
$ myptcl deposit 0 99 1
$ mkdir myptcl
$ mv deposit_particle_*.plt myptcl
$ cd myptcl
$ /usr/local/tecplot/360ex_2021r1/bin/tec360 &
```
then load data and apply macro `ptcl_grid_bdry.mcr` optionally.

The above command `myptcl deposit 0 99 1` for serial process can be replaced by the 
following parallel processing:
```
$ myptcl.para deposit 0 99 1 10
```
which uses 10 processes for parallel computing.

or use a different executable, which generates binary directly with higher performance:

```
$ export PATH=/usr/local/tecplot/360ex_2021r1/bin:$PATH
$ myptclbin deposit 0 99 1
$ mkdir myptclbin
$ mv deposit_*.plt myptclbin
$ cd myptclbin
$ /usr/local/tecplot/360ex_2021r1/bin/tec360 &
```
then load data and apply macro `ptcl_grid_bdry_bin.mcr` optionally.

The above command `myptclbin deposit 0 99 1` for serial process can be replaced by the 
following parallel processing:
```
$ myptclbin.para deposit 0 99 1 10
```
which uses 10 processes for parallel computing.

Note that `preplot`, the free Tecplot tool, is used in the scripts.

#### To visualize particles' displacement/force/velocity
To plot displacement/force/velocity vectors for particles, follow these steps:
```
$ plotvec deposit_particle 0 99 1
$ mkdir vec;mv vec_deposit_particle_*.dat vec; cd vec
$ mypre
$ /usr/local/tecplot/360ex_2021r1/bin/tec360 &
```
The data can be processed in parallel, for example:
```
$ plotvec.para deposit_particle 0 99 1 10
$ mkdir vec;mv vec_deposit_particle_*.dat vec; cd vec
$ mypre.para vec_deposit_particle 0 99 1 10
$ /usr/local/tecplot/360ex_2021r1/bin/tec360 &
```
The particles can also be visualized as uniform spheres or other shapes using Tecplot scatter.

#### To visualize interparticle contacts
To plot interparticle and particle-boundary contact information for an isotropic compressing test, 
first turn on the "PRINT_CONTACT" option in `makefile` for compiling and execution, then follow these steps:
```console
$ uniqueContact isotropic_contact 0 99 1
$ uniqueContact isotropic_contact_end
$ calcStressAll isotropic_contact isotropic_bdrycntc 0 99 1
$ calcStressAll isotropic_contact_end isotropic_bdrycntc_end 

$ mkdir contacts
$ plotContact isotropic_contact 0 99 1
$ plotBdryContact isotropic_bdrycntc 0 99 1
$ mv isotropic_contact_*.dat isotropic_bdrycntc_*.dat contacts
$ cd contacts/
$ plotContactAll isotropic_contact isotropic_bdrycntc 0 99 1
$ mkdir all; mv isotropic_contact_*all.dat all; cd all
$ mypre
$ mv *.plt .. ; cd ..
$ /usr/local/tecplot/360ex_2021r1/bin/tec360 &
```

To plot particle-boundary contact information only, you only need to run:
```console
$ plotBdryContact isotropic_bdrycntc 0 99 1
```

#### To plot statistics
To plot statistics information for an isotropic compressing test, follow these steps using Matlab scripts:
```console
$ matlab &
dem_isotropic_stress_strain('isotropic_progress')
dem_isotropic_force_energy('isotropic_progress')
dem_isotropic_stiffness_energe('isotropic_progress')
dem_isotropic_shear('isotropic_progress')
```

### For DEM-CFD
The DEM data are processed the same way as above. For CFD data, the steps are as follows:

1\. For data obtained from parallel computing, run sortParaIJK to sort
data and replace themselves (note that sortParaIJK also removes IJK
indexing); do NOT run sortParaIJK for data obtained from serial
computing as they do not need sorting. For example,
```console
$ sortParaIJK
-- sort unordered IJK data obtained from parallel IO --
-- note the original files are to be replaced --
  Usage:  sortParaIJK  file_prefix  start_snap  end_snap  increment
Example:  sortParaIJK  couple_fluidplot  0  100  1

$ sortParaIJK.para
purpose: sort in parallel unordered IJK data obtained from parallel IO
         note the original files are to be replaced
  usage: sortParaIJK.para prefix start_snap end_snap increment number_of_process
example: sortParaIJK.para couple_fluidplot 1 100 5 10
```

2\. convert CFD data to Tecplot format
```console
$ mygas
purpose: convert CFD data to Tecplot format
  usage: mygas prefix start_snap end_snap increment
example: mygas couple_fluidplot 1 100 5

$ mygas.para
purpose: convert CFD to Tecplot format in parallel
  usage: mygas.para prefix start_snap end_snap increment number_of_process
example: mygas.para couple_fluidplot 1 100 5 10
```

3\. run tecplot to read binary data and begin rendering.

4\. create slice view in Tecplot: contour layout ->data ->extract >extract slices over time ->save as .lay and .plt.

## How to cite

If you use this software in academic research, please consider citing the following:

``` bibtex
@misc{paraellip3d-cfd,
  author={Yan, Beichuan and Lee, Yu-Hsuan},
  title={{ParaEllip3d-CFD}},
  year={2020},
  url={https://gitlab.com/micromorph/paraellip3d-cfd}
}
```

## Contributing

Contributions to ParaEllip3d-CFD are encouraged and we will work with you to
plan contributions, review, and merge. See the [Contributing
guide](CONTRIBUTING.md) for an explanation of the process and best practices for
contributing.

Please observe the [Code of Conduct](CODE_OF_CONDUCT.md) in all interactions
with the ParaEllip3d-CFD community.

## License

ParaEllip3d-CFD is available under your choice of the [Apache 2.0
license](LICENSE-APACHE) and/or the [MIT license](LICENSE-MIT). Copyright is
held by the University of Colorado and other contributors. See the version
control history for details.

## Acknowledgment
The development of `ParaEllip3d-CFD` was supported by the Department of Defense ONR MURI 
grant N00014-11-1-0691 (2011-2016): 
*Integrated Experimental-Computational Multiscale Immersed Particle-Continuum Approach 
to Modeling and Simulation of Multiphase Soil Failure Mechanics under Buried Explosive Loading*,
and ONR grant N00014-17-1-2704 (2017-2020): 
*Versatile Hybrid Multi-scale Multi-phase Multi-method Computational-experimental 
Platform for Simulating Explosive Loading of Soils*.
The DoD High Performance Computing Modernization Program (HPCMP) granted computing 
resources to conduct the work.

This work is currently supported by the Department of Energy, National Nuclear Security 
Administration (DOE/NNSA), award number DE-NA0003962: 
*Multi-disciplinary Simulation Center (MSC) for Micromorphic Multiphysics Porous and 
Particulate Materials Simulations Within Exascale Computing Workflows*, a cooperative 
project of Advanced Simulation and Computing Predictive Science Academic Alliance Program (PSAAP III).

## Contact
Beichuan Yan (CU Boulder), Email: beichuan.yan@colorado.edu  
Yu-Hsuan Lee (CU Boulder), Email: yuhsuan.lee@colorado.edu  
Richard Regueiro (CU Boulder), Email: richard.regueiro@colorado.edu  
Jed Brown (CU Boulder), Email: jed.brown@colorado.edu  
