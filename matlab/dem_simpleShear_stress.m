function dem_simpleShear_stress(file_prefix, start, endsnap, stride, element_number, plot_p)

screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

processors=element_number;
snapshots=floor((endsnap-start)/stride)+1;
stress_tensor=zeros(processors,snapshots,3,3);
shearStrain=zeros(1,snapshots);
savetype='.png';
nametext=[char(file_prefix) '_tensor_'];
boundary_nametext=[char(file_prefix) '_boundary_'];
k=0;
for i=start:stride:endsnap
    if i<10
        filename=[nametext '00' num2str(i)];
        boundary_filename=[boundary_nametext '00' num2str(i)];
    elseif i<100
        filename=[nametext '0' num2str(i)];
        boundary_filename=[boundary_nametext '0' num2str(i)];
    else
        filename=[nametext num2str(i)];
        boundary_filename=[boundary_nametext num2str(i)];
    end
    new_boundary_data=importdata(boundary_filename);
    k=k+1;
    shearStrain(k)=atan(new_boundary_data(10,4)/new_boundary_data(10,3));
    data=importdata(filename);
    celldata=data.textdata;
    for j=1:processors
    stress_tensor(j,k,1,1)=str2double(celldata(3+69*(j-1),1));
    stress_tensor(j,k,1,2)=str2double(celldata(3+69*(j-1),2));
    s=celldata{3+69*(j-1),3};
    stress_tensor(j,k,1,3)=str2double(s(1:size(s,2)-1));
    stress_tensor(j,k,2,1)=str2double(celldata(4+69*(j-1),1));
    stress_tensor(j,k,2,2)=str2double(celldata(4+69*(j-1),2));
    s=celldata{4+69*(j-1),3};
    stress_tensor(j,k,2,3)=str2double(s(1:size(s,2)-1));
    stress_tensor(j,k,3,1)=str2double(celldata(5+69*(j-1),1));
    stress_tensor(j,k,3,2)=str2double(celldata(5+69*(j-1),2));
    s=celldata{5+69*(j-1),3};
    stress_tensor(j,k,3,3)=str2double(s(1:size(s,2)-1));
    end
end

 for j=1:processors  
 if plot_p ~= 'all'
     j=plot_p+1;
 end
 
 %figure(1)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on');
 plot(shearStrain,stress_tensor(j,:,1,1),'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,1,1)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{xx} (Pa)')
 if (max(stress_tensor(j,:,1,1))>0)&&(min(stress_tensor(j,:,1,1))<0)
 elseif max(stress_tensor(j,:,1,1))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,1,1))<0
     ylim([-inf 0])
 end
 title(['\sigma_{xx} (processor ' num2str(j-1) ')'])
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_xx_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);
 
 %figure(2)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on'); 
 plot(shearStrain,stress_tensor(j,:,2,2),'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,2,2)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{yy} (Pa)')
 title(['\sigma_{yy} (processor ' num2str(j-1) ')'])
 if (max(stress_tensor(j,:,2,2))>0)&&(min(stress_tensor(j,:,2,2))<0)
 elseif max(stress_tensor(j,:,2,2))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,2,2))<0
     ylim([-inf 0])
 end
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_yy_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);
 
 %figure(3)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on'); 
 plot(shearStrain,stress_tensor(j,:,3,3),'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,3,3)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{zz} (Pa)')
 title(['\sigma_{zz} (processor ' num2str(j-1) ')'])
 if (max(stress_tensor(j,:,3,3))>0)&&(min(stress_tensor(j,:,3,3))<0)
 elseif max(stress_tensor(j,:,3,3))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,3,3))<0
     ylim([-inf 0])
 end
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_zz_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);
 
 %figure(4)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on'); 
 plot(shearStrain,(stress_tensor(j,:,1,2)+stress_tensor(j,:,2,1))/2,'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,1,2)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{xy} (Pa)')
 title(['\sigma_{xy} (processor ' num2str(j-1) ')'])
 if (max(stress_tensor(j,:,1,2))>0)&&(min(stress_tensor(j,:,1,2))<0)
 elseif max(stress_tensor(j,:,1,2))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,1,2))<0
     ylim([-inf 0])
 end
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_xy_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);

 %figure(5)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on'); 
 plot(shearStrain,(stress_tensor(j,:,1,3)+stress_tensor(j,:,3,1))/2,'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,1,3)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{xz} (Pa)')
 title(['\sigma_{xz} (processor ' num2str(j-1) ')'])
 if (max(stress_tensor(j,:,1,3))>0)&&(min(stress_tensor(j,:,1,3))<0)
 elseif max(stress_tensor(j,:,1,3))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,1,3))<0
     ylim([-inf 0])
 end
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_xz_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);

 %figure(6)
 fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
 set(fh, 'visible', 'on'); 
 plot(shearStrain,(stress_tensor(j,:,2,3)+stress_tensor(j,:,3,2))/2,'LineWidth',3)
 if (shearStrain(k)<0)
     set(gca,'XDir','reverse')
 end
 if stress_tensor(j,k,2,3)<0
     set(gca,'YDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma_{yz} (Pa)')
 title(['\sigma_{yz} (processor ' num2str(j-1) ')'])
 if (max(stress_tensor(j,:,2,3))>0)&&(min(stress_tensor(j,:,2,3))<0)
 elseif max(stress_tensor(j,:,2,3))>0
     ylim([0 inf])
 elseif min(stress_tensor(j,:,2,3))<0
     ylim([-inf 0])
 end
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 filename=[file_prefix '_stress_yz_processor_' num2str(j-1) savetype];
 %saveas(gcf,filename)
 set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
 set(gcf, 'paperpositionmode', 'auto');
 options.Format = 'png';
 hgexport(fh, filename, options);
 
 if plot_p ~= 'all'
     break
 end

 end
