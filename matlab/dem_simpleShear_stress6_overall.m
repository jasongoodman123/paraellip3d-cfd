function dem_simpleShear_stress6_overall(file_prefix, start, endsnap, stride)

screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

snapshots=floor((endsnap-start)/stride)+1;
stress_tensor=zeros(snapshots,3,3);
shearStrain=zeros(1,snapshots);
nametext=[char(file_prefix) '_contact_stress_'];
boundary_nametext=[char(file_prefix) '_boundary_'];
k=0;
for i=start:stride:endsnap
    if i<10
        filename=[nametext '00' num2str(i)];
        boundary_filename=[boundary_nametext '00' num2str(i)];
    elseif i<100
        filename=[nametext '0' num2str(i)];
        boundary_filename=[boundary_nametext '0' num2str(i)];
    else
        filename=[nametext num2str(i)];
        boundary_filename=[boundary_nametext num2str(i)];
    end
    new_boundary_data=importdata(boundary_filename);
    k=k+1;
    shearStrain(k)=atan(new_boundary_data(10,4)/new_boundary_data(10,3));
    data=importdata(filename);
    for j=1:3
      s=data{j+2,1};
      if (s(3)=='-')
        stress_tensor(k,j,1)=str2double(s(3:15));
      else
        stress_tensor(k,j,1)=str2double(s(4:15));
      end
      if (s(18)=='-')
        stress_tensor(k,j,2)=str2double(s(18:30));
      else
        stress_tensor(k,j,2)=str2double(s(19:30));
      end
      if (s(33)=='-')
        stress_tensor(k,j,3)=str2double(s(33:45));
      else
        stress_tensor(k,j,3)=str2double(s(34:45));
      end
    end 
end

fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');

subplot(1,1,1)
 %figure(1)
 %hold on
 %plot(shearStrain,-stress_tensor(:,1,1),'LineWidth',3)
 %plot(shearStrain,-stress_tensor(:,2,2),'LineWidth',3)
 %plot(shearStrain,-stress_tensor(:,3,3),'LineWidth',3)
 %plot(shearStrain,(stress_tensor(:,1,2)+stress_tensor(:,2,1))/2,'LineWidth',3)
 %plot(shearStrain,(stress_tensor(:,1,3)+stress_tensor(:,3,1))/2,'LineWidth',3)
 %plot(shearStrain,(stress_tensor(:,2,3)+stress_tensor(:,3,2))/2,'LineWidth',3)
 forceH = plot(shearStrain,-stress_tensor(:,1,1), 'r', shearStrain,-stress_tensor(:,2,2), 'b', shearStrain,-stress_tensor(:,3,3), 'g', shearStrain,(stress_tensor(:,1,2)+stress_tensor(:,2,1))/2, 'k', shearStrain,(stress_tensor(:,1,3)+stress_tensor(:,3,1))/2, 'c', shearStrain,(stress_tensor(:,2,3)+stress_tensor(:,3,2))/2, 'm','LineWidth', 3); 
 if (shearStrain(k)<0)
   set(gca,'XDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma (Pa)')
 title('Stress')
 legend('\sigma_{xx}','\sigma_{yy}','\sigma_{zz}','\sigma_{xy}','\sigma_{xz}','\sigma_{yz}')
 legend('boxoff')
 grid on;
 grid minor;
 pbaspect([1 1 1]);
 
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(file_prefix, '.png'), 'png');
options.Format = 'png';
hgexport(fh, strcat(file_prefix, '_stress6_overall.png'), options);
