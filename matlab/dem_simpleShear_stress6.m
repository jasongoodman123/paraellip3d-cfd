function dem_simpleShear_stress6(file_prefix, start, endsnap, stride, element_number, plot_p)

screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

processors=element_number;
snapshots=floor((endsnap-start)/stride)+1;
stress_tensor=zeros(processors,snapshots,3,3);
shearStrain=zeros(1,snapshots);
savetype='.png';
nametext=[char(file_prefix) '_tensor_'];
boundary_nametext=[char(file_prefix) '_boundary_'];
k=0;
for i=start:stride:endsnap
    if i<10
        filename=[nametext '00' num2str(i)];
        boundary_filename=[boundary_nametext '00' num2str(i)];
    elseif i<100
        filename=[nametext '0' num2str(i)];
        boundary_filename=[boundary_nametext '0' num2str(i)];
    else
        filename=[nametext num2str(i)];
        boundary_filename=[boundary_nametext num2str(i)];
    end
    new_boundary_data=importdata(boundary_filename);
    k=k+1;
    shearStrain(k)=atan(new_boundary_data(10,4)/new_boundary_data(10,3));
    data=importdata(filename);
    celldata=data.textdata;
    for j=1:processors
    stress_tensor(j,k,1,1)=str2double(celldata(3+69*(j-1),1));
    stress_tensor(j,k,1,2)=str2double(celldata(3+69*(j-1),2));
    s=celldata{3+69*(j-1),3};
    stress_tensor(j,k,1,3)=str2double(s(1:size(s,2)-1));
    stress_tensor(j,k,2,1)=str2double(celldata(4+69*(j-1),1));
    stress_tensor(j,k,2,2)=str2double(celldata(4+69*(j-1),2));
    s=celldata{4+69*(j-1),3};
    stress_tensor(j,k,2,3)=str2double(s(1:size(s,2)-1));
    stress_tensor(j,k,3,1)=str2double(celldata(5+69*(j-1),1));
    stress_tensor(j,k,3,2)=str2double(celldata(5+69*(j-1),2));
    s=celldata{5+69*(j-1),3};
    stress_tensor(j,k,3,3)=str2double(s(1:size(s,2)-1));
    end
end

 for j=1:processors  
 if plot_p ~= 'all'
     j=plot_p+1;
 end
 
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');

subplot(1,1,1) 
 %figure(1)
 %hold on
 %plot(shearStrain,-stress_tensor(j,:,1,1),'LineWidth',3)
 %plot(shearStrain,-stress_tensor(j,:,2,2),'LineWidth',3)
 %plot(shearStrain,-stress_tensor(j,:,3,3),'LineWidth',3)
 %plot(shearStrain,(stress_tensor(j,:,1,2)+stress_tensor(j,:,2,1))/2,'LineWidth',3)
 %plot(shearStrain,(stress_tensor(j,:,1,3)+stress_tensor(j,:,3,1))/2,'LineWidth',3)
 %plot(shearStrain,(stress_tensor(j,:,2,3)+stress_tensor(j,:,3,2))/2,'LineWidth',3)
 forceH = plot(shearStrain,-stress_tensor(j,:,1,1), 'r', shearStrain,-stress_tensor(j,:,2,2), 'b', shearStrain,-stress_tensor(j,:,3,3), 'g', shearStrain,(stress_tensor(j,:,1,2)+stress_tensor(j,:,2,1))/2, 'k', shearStrain,(stress_tensor(j,:,1,3)+stress_tensor(j,:,3,1))/2, 'c', shearStrain,(stress_tensor(j,:,2,3)+stress_tensor(j,:,3,2))/2, 'm','LineWidth', 3); 
 if (shearStrain(k)<0)
   set(gca,'XDir','reverse')
 end
 xlabel('\gamma_{yz} (rad)')
 ylabel('\sigma (Pa)')
 title(['Stress (processor ' num2str(j-1) ')'])
 legend('\sigma_{xx}','\sigma_{yy}','\sigma_{zz}','\sigma_{xy}','\sigma_{xz}','\sigma_{yz}')
 legend('boxoff')
 grid on;
 grid minor;
 pbaspect([1 1 1]);

 
 if plot_p ~= 'all'
     break
 end

 end

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(file_prefix, '.png'), 'png');
options.Format = 'png';
hgexport(fh, [file_prefix '_stress6' '_processor' num2str(plot_p)], options);
