function dem_simpleShear_tractions_multiConfining(fileToRead1)
screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

%read valid directories
dirall=dir('.');
dirs=dirall([dirall(:).isdir]);
dirs=dirs(~ismember({dirs.name},{'.','..'}));
n_confining=size(dirs,1);

%define arrays of unknown sizes
max_row = 100000;
traction_x=zeros(max_row, n_confining);
traction_y=zeros(max_row, n_confining);
traction_z=zeros(max_row, n_confining);
shear_x=zeros(max_row, n_confining);
shear_y=zeros(max_row, n_confining);
shear_z=zeros(max_row, n_confining);
total_y=zeros(max_row, n_confining);
total_z=zeros(max_row, n_confining);

%for each confining pressure, read its data and store in arrays
for CF=1:n_confining
 filename=[dirs(CF).name '/' fileToRead1];       
 timeStep = 1; %5e-7;
 newData1 = importdata(filename);
 %create new variables in the caller workspace from those fields.
 for i = 1:size(newData1.colheaders, 2)
  assignin('caller', genvarname(newData1.colheaders{i}), newData1.data(:,i));
 end

step = evalin('caller', 'iteration');
traction_x1 = evalin('caller', 'traction_x1');
traction_x2 = evalin('caller', 'traction_x2');
traction_y1 = evalin('caller', 'traction_y1');
traction_y2 = evalin('caller', 'traction_y2');
traction_z1 = evalin('caller', 'traction_z1');
traction_z2 = evalin('caller', 'traction_z2');
mean_stress  = evalin('caller', 'mean_stress ');
shear_x1 = evalin('caller', 'shear_x1');
shear_x2 = evalin('caller', 'shear_x2');
shear_y1 = evalin('caller', 'shear_y1');
shear_y2 = evalin('caller', 'shear_y2');
shear_z1 = evalin('caller', 'shear_z1');
shear_z2 = evalin('caller', 'shear_z2');
shearAngle = evalin('caller', 'shearAngle');
bulk_volume = evalin('caller', 'bulk_volume');
density = evalin('caller', 'density');
epsilon_x = evalin('caller', 'epsilon_x');
epsilon_y = evalin('caller', 'epsilon_y');
epsilon_z = evalin('caller', 'epsilon_z');
epsilon_v = evalin('caller', 'epsilon_v');
void_ratio = evalin('caller', 'void_ratio');
porosity = evalin('caller', 'porosity');
avgNormal = evalin('caller', 'avgNormal');
avgShear = evalin('caller', 'avgShear');
avgPenetr = evalin('caller', 'avgPenetr');
transEnergy = evalin('caller', 'transEnergy');
rotatEnergy = evalin('caller', 'rotatEnergy');
kinetEnergy = evalin('caller', 'kinetEnergy');
accruedTime = step * timeStep;

rowNum = size(traction_x1,1);
for i = 1:rowNum
 traction_x(i,CF) = (traction_x1(i) + traction_x2(i) ) / 2.0;
 traction_y(i,CF) = (traction_y1(i) + traction_y2(i) ) / 2.0;
 traction_z(i,CF) = (traction_z1(i) + traction_z2(i) ) / 2.0; 
 shear_x(i,CF) = (shear_x1(i)  + shear_x2(i) ) / 2.0;
 shear_y(i,CF) = (shear_y1(i)  + shear_y2(i) ) / 2.0;
 shear_z(i,CF) = (shear_z1(i)  + shear_z2(i) ) / 2.0;
 total_y(i,CF) = sqrt(traction_y(i,CF)^2 + shear_y(i,CF)^2);
 total_z(i,CF) = sqrt(traction_z(i,CF)^2 + shear_z(i,CF)^2);
end

end
%end of reading all data and storing in arrays.

%start plotting
%figure(1)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
subplot(1,2,1)
for CF=1:n_confining
 plot(shearAngle, traction_y(1:rowNum,CF), 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Normal tractions (left/right)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

subplot(1,2,2)
for CF=1:n_confining
 plot(shearAngle, shear_y(1:rowNum,CF), 'LineWidth', 3); 
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Shear tractions (left/right)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, 'simpleShear_tractionMultiConf_y', options);
hold off;

%figure(2)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
subplot(1,2,1)
for CF=1:n_confining
 plot(shearAngle, traction_z(1:rowNum,CF), 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Normal tractions (top/bottom)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

subplot(1,2,2)
for CF=1:n_confining
 plot(shearAngle, shear_z(1:rowNum,CF), 'LineWidth', 3); 
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Shear tractions (top/bottom)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, 'simpleShear_tractionMultiConf_z', options);
hold off;

%figure(3)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
subplot(1,2,1)
for CF=1:n_confining
 plot(shearAngle, traction_x(1:rowNum,CF), 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Normal tractions (front/back)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

subplot(1,2,2)
for CF=1:n_confining
 plot(shearAngle, shear_x(1:rowNum,CF), 'LineWidth', 3); 
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Shear tractions (front/back)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, 'simpleShear_tractionMultiConf_x', options);
hold off;

% a special plot: only for x normal tractions, to be compared with sigma_xx
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
subplot(1,1,1)
for CF=1:n_confining
 plot(shearAngle, traction_x(1:rowNum,CF), 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Normal tractions (front/back)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%ylim([0, 10e5]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, 'simpleShear_tractionMultiConf_x_normal', options);
hold off;
% end of a special plot: only for x normal tractions, to be compared with sigma_xx

%figure(4)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
subplot(1,2,1)
for CF=1:n_confining
 plot(shearAngle, total_y(1:rowNum,CF), 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Total tractions (left/right)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

subplot(1,2,2)
for CF=1:n_confining
 plot(shearAngle, total_z(1:rowNum,CF), 'LineWidth', 3); 
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Total tractions (top/bottom)');
xlabel('Shear angle (rad)');
ylabel('Traction (N/m^2)');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
%xlim([0, 4000]);

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, 'simpleShear_tractionMultiConf_yz', options);
hold off;

