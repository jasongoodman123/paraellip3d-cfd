function dem_simpleShear_stress_multiConfining_overall(file_prefix, start, endsnap, stride)

screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

dirall=dir('.');
dirs=dirall([dirall(:).isdir]);
n_confining=size(dirs,1)-2;
snapshots=floor((endsnap-start)/stride)+1;
stress_tensor=zeros(snapshots,n_confining,3,3);
shearStrain=zeros(1,snapshots);
savetype='.png';
nametext=['/' char(file_prefix) '_contact_stress_'];
boundary_nametext=['/' char(file_prefix) '_boundary_'];
for CF=1:n_confining
k=0;
for i=start:stride:endsnap
    if i<10
        filename=[dirs(CF+2).name nametext '00' num2str(i)];
        boundary_filename=[dirs(CF+2).name boundary_nametext '00' num2str(i)];
    elseif i<100
        filename=[dirs(CF+2).name nametext '0' num2str(i)];
        boundary_filename=[dirs(CF+2).name boundary_nametext '0' num2str(i)];
    else
        filename=[dirs(CF+2).name nametext num2str(i)];
        boundary_filename=[dirs(CF+2).name boundary_nametext num2str(i)];
    end
    new_boundary_data=importdata(boundary_filename);
    k=k+1;
    shearStrain(k)=atan(new_boundary_data(10,4)/new_boundary_data(10,3));
    data=importdata(filename);
    for j=1:3
      s=data{j+2,1};
      if (s(3)=='-')
        stress_tensor(k,CF,j,1)=str2double(s(3:15));
      else
        stress_tensor(k,CF,j,1)=str2double(s(4:15));
      end
      if (s(18)=='-')
        stress_tensor(k,CF,j,2)=str2double(s(18:30));
      else
        stress_tensor(k,CF,j,2)=str2double(s(19:30));
      end
      if (s(33)=='-')
        stress_tensor(k,CF,j,3)=str2double(s(33:45));
      else
        stress_tensor(k,CF,j,3)=str2double(s(34:45));
      end
    end 
end
end

%figure(1)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,1,1),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,1,1)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,1,1))>0)&&(min(stress_tensor(:,CF,1,1))<0)
elseif max(stress_tensor(:,CF,1,1))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,1,1))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{xx} (Pa)')
title('\sigma_{xx}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_xx_overall' savetype];
hgexport(fh, filename, options);
hold off

%figure(2)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,2,2),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,2,2)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,2,2))>0)&&(min(stress_tensor(:,CF,2,2))<0)
elseif max(stress_tensor(:,CF,2,2))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,2,2))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{yy} (Pa)')
title('\sigma_{yy}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_yy_overall' savetype];
hgexport(fh, filename, options);
hold off

%figure(3)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,3,3),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,3,3)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,3,3))>0)&&(min(stress_tensor(:,CF,3,3))<0)
elseif max(stress_tensor(:,CF,3,3))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,3,3))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{zz} (Pa)')
title('\sigma_{zz}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_zz_overall' savetype];
hgexport(fh, filename, options);
hold off

%figure(4)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,1,2),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,1,2)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,1,2))>0)&&(min(stress_tensor(:,CF,1,2))<0)
elseif max(stress_tensor(:,CF,1,2))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,1,2))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{xy} (Pa)')
title('\sigma_{xy}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_xy_overall' savetype];
hgexport(fh, filename, options);
hold off

%figure(5)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,1,3),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,1,3)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,1,3))>0)&&(min(stress_tensor(:,CF,1,3))<0)
elseif max(stress_tensor(:,CF,1,3))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,1,3))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{xz} (Pa)')
title('\sigma_{xz}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_xz_overall' savetype];
hgexport(fh, filename, options);
hold off

%figure(6)
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');
for CF=1:n_confining
    subplot(1,1,1)
    plot(shearStrain,stress_tensor(:,CF,2,3),'LineWidth',3)
    hold on
end
if (shearStrain(k)<0)
    set(gca,'XDir','reverse')
end
if stress_tensor(k,CF,2,3)<0
    set(gca,'YDir','reverse')
end
if (max(stress_tensor(:,CF,2,3))>0)&&(min(stress_tensor(:,CF,2,3))<0)
elseif max(stress_tensor(:,CF,2,3))>0
    ylim([0 inf])
elseif min(stress_tensor(:,CF,2,3))<0
    ylim([-inf 0])
end
xlabel('\gamma_{yz} (rad)')
ylabel('\sigma_{yz} (Pa)')
title('\sigma_{yz}')
legend(dirs(3:end).name)
legend('boxoff')
grid on;
grid minor;
pbaspect([1 1 1]);
set(findall(gcf, '-property', 'fontSize'), 'fontSize', 26, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
options.Format = 'png';
filename=[file_prefix '_stressMultiConf_yz_overall' savetype];
hgexport(fh, filename, options);
hold off

